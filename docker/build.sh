#!/bin/bash

# @blame R. Matt McCann <mccann.matt@gmail.com>
# @brief Builds the developer Docker image
# @copyright &copy; 2017 Human-Proof Corp.

group_id=`id -rg`
group_name=`id -rgn`
user_id=`id -ru`
user_name=`id -run`

cp docker/Dockerfile .
docker build -t human-proof/libreceiver \
    --build-arg GROUP_ID=$group_id \
    --build-arg GROUP_NAME=$group_name \
    --build-arg USER_ID=$user_id \
    --build-arg USER_NAME=$user_name \
    . || echo "docker build failed"
rm Dockerfile
