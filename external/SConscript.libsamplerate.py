'''
@blame R. Matt McCann <mccann.matt@gmail.com>
@brief Build script for the samplerate library
@copyright 2017 Evil Corp.
'''

import os

from SCons.Script import Action, SideEffect

from misc.build_helper import CleanConfigureMake, ConfigureMake, get_environment


# If the configure script hasn't been generated
if not os.path.exists('configure'):
    # Run the autogen script
    os.system('./autogen.sh')

# Get the build environment
env = get_environment()

# Configure the build of libsamplerate
targets = [
    'samplerate',
    '#lib/libsamplerate.a',
    '#lib/libsamplerate.so',
    '#lib/libsamplerate.so.1'
]
samplerate = env.Command(
        target=targets,
        source="configure",
        action=Action(ConfigureMake, None)
    )
CleanConfigureMake(
    target=targets,
    source="configure",
    env=env
)
env.Alias('samplerate', samplerate)

# Tell SCons that building samplerate produces the header file required to compile receiver
SideEffect('samplerate_headers', samplerate)
