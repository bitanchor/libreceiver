function dump_cvec(data, file_name)
    % Dumps the contents of the complex vector data out to the
    % specified file in a format accepted by the C++ unit tests.

    dump_file = fopen(file_name, 'w');
    fprintf(dump_file, '%d\n', length(data));
    for iter = 1:length(data)
        fprintf(dump_file, '%5.15f %5.15f\n', real(data(iter)), imag(data(iter)));
    end
    fclose(dump_file);
end
