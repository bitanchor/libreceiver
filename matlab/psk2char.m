function str = psk2char_potter(y,M)
% psk2char.m
% Function to decode M-PSK symbols to a string of ASCII text.
% (See also char2psk.m)
%
% str    output string variable with text
% M      size of PSK constellation (2 or 4; or 'bpsk or 'qpsk')
% y      list of PSK IQ points (complex numbers)
 
% ECE4007
% Au2014

%% error checks
if(nargin ~= 2)
    error('Error: the function psk2char.m requires two input arguments')
end
if (isnumeric(y) ~= 1 || isempty(y)) 
    error('Error: the input y must be numeric.')
end

%% bit slicing
% first, symbol to integer label
switch M
    case {2,'bpsk','BPSK'}
        M = 2;
        label = (sign(real(y))+1)/2;
    case {4,'qpsk','QPSK'}
        M = 4;
        %real is MSB for QPSK
        label = 2*(real(y) >= 0) + (imag(y) >= 0);  
end
% second, label to bits
N = length(label)*log2(M)/8; %number of characters
strbin = reshape(dec2bin(label,log2(M)).',8,N).';%array, 8 bits per character
% third, bits to characters
str=char(bin2dec(strbin))';% convert binary to decimal ASCII to character
% end of function