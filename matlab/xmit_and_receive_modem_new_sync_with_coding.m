function xmit_and_receive_modem_new_sync_with_coding(num_packets)
    %Here's where we define the parameters of the system
    %total_time=1; %window of time that the receiver listens to for the signal.

    %The next four variables are for the MATLAB audio interface
    Nbits=16; %bits per sample at the input of the receiver for the observed audio signal
    NChannels=1; %1 for mono, 2 for stereo for audio
    amplification_factor=5; %gain of the speaker volume
    Fs=41992; %sampling rate in samples per second

    %Here, the recording object is created. This sets up the connection between
    %the soundcard and the mic. output:
    recObj=audiorecorder(Fs,Nbits,NChannels);


    %Next, we start setting up the communication parameters:
    sym_rate=724; %symbol rate in symbols per second. choose Fs/sym_rate to be an integer.
    max_len=255; %data sequence length in number of symbols - no more than 255 symbols
    pulse_len=2049; %length of the baseband pulses in number of samples. Choose this as 2^k+1 for some integer k.
    fc=19000; %carrier frequency %%%
    power_xmit=2e2; %transmission power level. Output signal level is proportional to this value
    ene=power_xmit/sym_rate; %energy per symbol. Note that, at a fixed power, this level goes down as the symbol rate goes up.



    %Next, we create the Turbo code object. There are three different codes
    %with decreasing code rates. Note that the lower code rates are more
    %reliable, however, the rate in bits/sec. decreases with lower code rates
    %as well.
    %One VERY IMPORTANT thing is: with each code, due to the interleaver
    %module, the length of the sequence of data bits can be of certain
    %length. The lengths can be listed by running the equation on MATLAB script
    %line. For instance, for the rate 1/3 code a list of packet lengths can be
    %found by typing 3*(1:200)-mod(3*(1:200)+12,8) on the command line.

    %poly2trellis = poly2trellis(4,[13 15],13); %rate 1/3 - No. of Trellis Termination Bits = 12
    %use packet sizes: 3*(1:200)-mod(3*(1:200)+12,8)
    %poly2trellis = poly2trellis(4,[13 15 17],13); %rate 1/5 - No. of Trellis Termination Bits = 18
    %use packet sizes: 5*(1:200)-mod(5*(1:200)+18,8)
    trellis = poly2trellis(4,[13 15 17 7],13); %rate 1/7 - No. of Trellis Termination Bits = 24
    %use packet sizes: 7*(1:200)-mod(7*(1:200)+24,8)


    %Here, we create the pilot sequence. The pilot sequence is BPSK, thus the
    %elements are always from the set {-1,1}.
    %A VERY IMPORTANT note is that, here we create it at random. But, for the
    %transmitter and the receiver being in separate machines, one has to make
    %sure that the pilot sequences match. Thus, you cannot randomize separately on
    %the transmitter and the receiver.
    %Also, another VERY IMPORTANT thing is, the pilot length cannot be too long
    %or too short, it cannot be too long, since the frequency mismatch can be
    %recovered if fc*freq_mismatch*pilot_len/sym_rate << 1. It cannot be too
    %short either, since the estimation quality degrades with shorter
    %sequences.
    %pilots=[1 1 1 1 1 -1 -1 1 1 -1 1 -1 1]; %form the unit-power pilot sequence
    %pilots=[pilots pilots -1];
    pilot_len=101; %length of the pilot sequence in number of symbols
    %%% pilots=sign(2*rand(1,pilot_len)-1); %pseudorandom pilot sequence.
    all_pilots=[1,1,-1,1,1,1,-1,-1,1,1,-1,-1,1,-1,1,-1,-1,1,1,1,-1,-1,-1,-1,-1,1,-1,-1,1,1,-1,-1,-1,-1,1,1,1,-1,-1,-1,1,-1,1,1,-1,1,-1,-1,1,1,1,1,-1,1,-1,1,1,1,-1,1,1,-1,1,1,1,-1,1,-1,1,-1,-1,1,1,-1,1,1,1,-1,-1,1,-1,-1,1,1,1,1,-1,-1,-1,-1,1,1,-1,1,-1,1,-1,1,-1,1,1]; %%%
    pilots = all_pilots(1:pilot_len); %%%

    %Next, we create the pulse shape. This is the signal that encapsulates each
    %symbol. We use squareroot raised cosine pulses. The IMPORTANT parameter
    %that one needs to be careful about is alpha, the bandwidth extension
    %factor. The bandwidth consumed by the signal is identical to
    %(1+alpha)*symbol rate. For instance, if the symbol rate is 1 ksymbols/sec,
    %and alpha=0.5, then the bandwidth consumed is 1.5 kHz. Choosing alpha
    %large is better for intersymbol interference, but worse for bandwidth
    %consumption.
    alpha=0.5; %bandwidth expansion factor
    lobes=(pulse_len-1)/(Fs/sym_rate); %number of sidelobes in the symbol
    pulse=rcosdesign(alpha,lobes,Fs/sym_rate); %plot this to see each pulse


    %Here's where the data sequence if formed. It is self explanatory in most
    %part. One can transmit a text message or randomly created data. Note that,
    %with random data, the code is slightly different if the transmitter and
    %the receiver are not colocated. Thus, instead of generating the data at
    %random each time, we use a separate pre-created file and load the data
    %from that file.
    %For that code with separate transmit and receive devices, please refer to the
    %codes under xmit_or_receive...
    %Constellation size can be 2 or 4 in this code. Note that, for acoustic
    %communication, these are more appropriate anyways, due to non-linear
    %effects causing distortions.
    %data_type = input('Text or Random data (T/R):','s');
    data_type = 'T';

    %If text:
    if data_type=='T'||data_type=='t',
        %%% modulation=input('Modulation BPSK(2) or 4-PSK(4)','s');
        modulation='2';
        con_size=str2num(modulation);
        %%% text_in=input('Enter text (max 256 characters):','s');
        text_in='hello'; %%%
        [qam_symbols_raw bits]=char2psk(text_in,con_size); %This line converts the text to constellation symbol.
        bit_len=length(bits)

    %If random data:
    elseif data_type=='R'||data_type=='r',
    %        modulation=input('Modulation BPSK(2), 4-PSK(4), or 16-QAM(16)','s');
            modulation=input('Modulation BPSK(2), 4-PSK(4)','s');
            con_size=str2num(modulation);
            load('data');
            dat_len=input('Length of the data sequence in number of bits (<1024)','s');
            bit_len=str2num(dat_len);
            bits=bits_rand(1:bit_len);
    else
            disp('invalid entry');
    end

    %At this point, we have the uncoded bits and the bit_len. Next, we need to
    %encode the bits, then create the signal to be transmitted.

    % Convert the string of ones and zeros characters into a vector of ones and zeros integers
    for i = 1:length(bits) %%%
        binary_vector(i) = str2num(bits(i)); %%%
    end %%%

    % This part Turbo encodes the data created:
    %%% intrlvrIndices = randperm(bit_len)
    intrlvrIndices = [18,11,16,39,29,32,19,37,14,5,26,20,17,4,7,15,12,40,3,10,9,6,1,38,21,34,24,36,13,31,8,25,22,35,33,23,2,27,30,28]; %%%
    turbo_encode = comm.TurboEncoder('TrellisStructure', trellis,'InterleaverIndices',intrlvrIndices);
    encodedData = step(turbo_encode,binary_vector');
    len_enc=length(encodedData);


    %Now we have the turbo encoded data and will map it to symbols (BPSK or QPSK sequence).
    %The output is a sequence of complex numbers (real numbers if BPSK).
    qam_symbols=char2psk(psk2char(2*encodedData(1:len_enc-mod(len_enc,8))-1,2),con_size);
    seq_len=length(qam_symbols); %data sequence length in number of symbols


    %This part is commented out, since we use constant-sized data packets. We
    %don't need to encode the length in the packet.
    %encode the length of the string to be sent via BPSK:
    % seq_len_bin=dec2bin(seq_len);
    % l=length(seq_len_bin);
    % if l<8;
    %     encoded_len(1:8-l)=-1;
    % end
    % for k=1:l,
    %     encoded_len(8-l+k)=2*bin2dec(seq_len_bin(k))-1;
    % end


    %Here, we form the SYMBOLS of the packet by combining the pilots and the
    %data symbols:
    packet=[pilots qam_symbols];

    %Here, we create the signal in the frame.
    % STEP 1: The packet is composed of symbols. By
    % upsample(packet,Fs/sym_rate), we create a sequence of Fs/sym_rate-1 0's
    % followed by an element of packet. Thus, the total number of samples as a
    % result is (Fs/sym_rate)xlength(packet)
    % STEP 2: Take the upsampled sequence of symbols and convolve it with the
    % pulse. This creates a sequence of pulses (of length (Fs/sym_rate) coming
    % one after another whose amplitudes are identical to the values of the
    % symbols. This process is called pulse amplitude modulation. The
    % amplitudes of the sequence of pulses are modulated by the symbols of the
    % packet.
    % STEP 3: By multiplying the signal with sqrt(ene), the overall energy of
    % each pulse is made identical to ene. Therefore, the transmission power is
    % power_xmit.
    baseband_packet=sqrt(ene)*conv(upsample(packet,Fs/sym_rate),pulse).';

    %Next, we will take the packet and create the FRAME: i.e., sequence of samples that
    %will form the transmitted signal over time window t.
    %start sample for the packet is chosen here. If we choose it at the 1/5 point,
    %the first 20% of the samples are blank 0's.
    %start_sample=floor(length(t)/10);
    %start_sample=randi([(pulse_len+1)/2 length(t)-((pilot_len+seq_len-1)*Fs/sym_rate+(pulse_len+1)/2)]);
    %initial_point=start_sample-(pulse_len-1)/2;
    initial_point=floor(length(baseband_packet)/5)

    total_time=(length(baseband_packet)+initial_point)/Fs

    %Time axis of the entire frame in discrete time with a size identical to the number of samples
    t=0:1/Fs:total_time-1/Fs;

    %This part could be ignored. I artificially generate a frequency mismatch
    %between the transmitter and the receiver to test the resilience. Set it to
    %0 in real implementation.
    %Complex carrier:
    freq_mismatch=-0.000; %fraction of frequency mismatch relative to center freq.
    %In order to be able to recover, we need fc*freq_mismatch*pilot_len/sym_rate << 1.
    f0=freq_mismatch*fc;
    fc_rec=fc+f0;
    sync_mismatch=0.00; %timing mismatch in carrier (fraction of a symbol)
    t0=sync_mismatch/sym_rate;
    xmit_carrier=exp(j*2*pi*fc*t); %complex transmit carrier
    rec_carrier=2*exp(-j*2*pi*fc_rec*(t-t0)); %complex receive carrier

    %Here, we first place the baseband signal starting at the initial point. Then
    %we modulate the baseband signal with the carrier. Ultimately, qam_signal is
    %centered around fc.
    qam_signal=zeros(size(t));
    qam_signal(initial_point:initial_point+length(baseband_packet)-1)=baseband_packet;
    qam_signal=real(xmit_carrier.*qam_signal);

    %channel_out = [amplification_factor * qam_signal];
    %channel_out = [channel_out channel_out channel_out];
    %audiowrite('../../multiple_keys.wav', channel_out, Fs);

    while num_packets > 0
        channel_out = [amplification_factor * qam_signal];
        num_packets = num_packets - 1;

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Option 1 - Play calculated audio over speaker, record with mic
        %
        % Start listening for the signal
        record(recObj);
        
        % Here we transmit the signal over the speaker
        %PlayObj = audioplayer(channel_out, Fs); %create the object
        %play(PlayObj);

        % Listen to the signal simultaneously at the mic.
        disp("Listening...");
        pause(3*total_time);
        stop(recObj);
        disp("Done listening...");
        channel_out = getaudiodata(recObj);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Option 2 - Feed calculated audio directly
        %channel_out = amplification_factor*qam_signal;
        %channel_out = channel_out';
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Option 3 - Load audio signal directly from key file
        %[channel_out, Fs] = audioread('../../noisy.wav');
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % Let's dynamically calculate time based off the size of the collected
        % audio sample, so that the script can gracefully handle audio signals
        % of varying lengths
        record_time = length(channel_out) / Fs;
        t = 0:1/Fs:record_time-1/Fs;

        %Now we have the output of the channel. We need to demodulate to recover
        %the signal at the baseband. Then, we need to 1. find where the content of
        %the signal lies, correct for the frequency mismatch and recover the
        %symbols. This is done using the pilots.
        %
        %The whole thing is done simultaneously. h_hat is the channel gain
        %estimate. signal_start_sample_hat is the estimate for the starting point
        %in number of samples of the data portion of the packet. pilot_start_sample_hat
        %is the estimate of the start sample of the pilots.

        % Note: All variables defined in a Matlabs script have global scoping,
        % meaning they are not cleaned up between runs of this loop even if
        % they are defined within the loop only! In order to avoid unexpected
        % multiple loop iteration behaviors, we must zero out all variables
        % defined in the loop!!
        counter = 0;
        fc_rec = fc;
        freq_shift_hat = 0;
        h_hat = 0;
        current_seq_start = 1;
        pilot_start_sample_hat = 0;

        %Here is where the loop starts. Demodulate the data with the carrier whose
        %center frequency is identical to the current estimate of the receive carrier
        %frequency. Initially starts at the transmitter frequency, but there will
        %be some mismatch. One of the objectives is to correct it.
        %
        % TODO(mmccann): There is some condition that allows the script to
        % become stuck in this while loop forever. Per Emre's suggestion,
        % adding a counter check to bail if this occurs
        while (current_seq_start ~= pilot_start_sample_hat) && (counter < 10)
            h_hat = 0;
            current_seq_start=pilot_start_sample_hat;
            rec_carrier=2*exp(-j*2*pi*fc_rec*t);
            demod_signal=rec_carrier.*channel_out';
            % This next step is to take the samples and form the sequence of
            % symbols. This is done in a few steps:
            % STEP 1: Convolve the the sequence of samples with the pulse:
            received_signal=conv(pulse,demod_signal).';
            % Remember that the number of samples is (Fs/sym_rate)xpacket
            % length. Thus the length of received_signal is too high. To obtain the
            % symbols of the packet, we need to find the sequence of samples in
            % received_signal that are separated by Fs/sym_rate. We can think of
            % this operation as a projection of the demodulated signal onto the
            % pulse shape. Thus the samples should be such that the projection is
            % maximized. That is what we do next:
            %
            % Here, we try to find the pilot sequence. For that, we look for the
            % sequence of pilot_len samples that are separated by
            % Fs/sym_rate that maximizes the projection onto the known pilot sequence.
            % That gives us the most likely sample sequence to form the symbols of
            % the packet: This next part identifies that most likely sequence:

            %for k=(pulse_len+1)/2:length(t)-((pilot_len+seq_len-1)*Fs/sym_rate+(pulse_len+1)/2), %potential center samples of any symbol
            for k=(pulse_len+1)/2:length(t)-(seq_len+pilot_len)*Fs/sym_rate, %potential center samples of any symbol
                potential_pilot_samples=k:Fs/sym_rate:k+(pilot_len-1)*Fs/sym_rate;
                %the above variable is the sequence of samples under investigation.
                h_cur=conj(pilots)*received_signal(potential_pilot_samples)/sqrt(ene)/pilot_len;
                if abs(h_cur)>abs(h_hat)  %in real time operation, change this condition to a threshold
                    h_hat=h_cur;
                    pilot_hat=received_signal(potential_pilot_samples)/sqrt(ene);
                    pilot_start_sample_hat=k;
                    signal_start_sample_hat=k+pilot_len*Fs/sym_rate;
                end
            end
            % The above process finds the best match for the pilot sequence. Now, for
            % that pilot sequence, we will estimate the frequency mismatch, correct for
            % it and recreate the demodulator carrier sequence. With that sequence we
            % will go over the above steps with the more coherent carriers.
            % To estimate the frequency mismatch, we fit the line to the sequence of
            % pilots/estimated pilots. The slope gives the frequency mismatch estimate.
            % The caveat here is the line match works only if the mismatch is below a
            % certain threshold to begin with. Hence, the requirement of
            % fc*freq_mismatch*pilot_len/sym_rate << 1 as above.

            y=angle(pilots./pilot_hat.');
            y1= y - (2*pi * floor((y + pi) / (2 * pi)));
            shifts=polyfit(0:2*pi/sym_rate:(pilot_len-1)*2*pi/sym_rate,y1,1);

            freq_shift_hat= freq_shift_hat + shifts(1)
            phase_shift_hat=shifts(2);

            %Correct for the mismatch and use the receive carrier with the corrected
            %frequency as you repeat this procedure.
            fc_rec=fc_rec-shifts(1);

            %counter merely counts for the number of iterations it takes to correct for
            %the mismatch. The LOOP TERMINATES when the same start sample is found in
            %two successive iterations.
            counter=counter+1;
        end


        %The following few lines regarding the wireless channel is a bit technical.
        %Here, I calculate the channel impulse
        %response in the discrete equivalent model. In a single tap channel, there
        %should be one significant tap (tap 0). You should see a plot that shows
        %the taps. If tap 0 is not substantially larger than the others, it means
        %there will be intersymbol interference due to multiple significant
        %reflectors in the environment. In such a case, please use the ofdm
        %versions of the same codes.

        %current_pilot=pilot_start_sample_hat

        %channel_impulse_response_hat=1/pilot_len*conv(pilot_hat,flip(conj(pilots)));
        %stem(-pilot_len+1:pilot_len-1,abs(channel_impulse_response_hat));
        %channel_taps=channel_impulse_response_hat(pilot_len:pilot_len+8);
        %stem(abs(channel_taps));

        %Similarly, the following is also a bit technical. I calculate a factor caused
        %by the phase rotation in correlator for frame timing
        %recovery. Works better when pilot length is odd.
        % If the correction factor is close to 1, then there is great match in
        % frequency and timing. If the mismatch is significant, the factor will be closer
        % to 0 and decoding will be almost impossible.
        correction_factor=1/pilot_len*(1+2*sum(cos(pi*freq_shift_hat/sym_rate*(pilot_len-2*(1:floor((pilot_len-1)/2))))))

        %%rotation=exp(j*2*pi*freq_shift_hat*((0:1/sym_rate:(8+seq_len-1)/sym_rate)+(pilot_len+1)/sym_rate/2)).';
        %rotation=exp(j*2*pi*shifts(1)*((0:1/sym_rate:(seq_len-1)/sym_rate))).';
        rotation=exp(j*2*pi*shifts(1)*((0:1/sym_rate:(length(channel_out))/sym_rate))).';

        %This next part was there for the case in which the length of the sequence
        %was decoded. Now, since the packet length is set constant and known at the
        %receiver, I commanded it out and merely set the seq_len_hat identical to
        %seq_len.
        %noisy_length=correction_factor/sqrt(ene)*rotation(1:8).*received_signal(signal_start_sample_hat:Fs/sym_rate:signal_start_sample_hat+(8-1)*Fs/sym_rate)/h_hat;
        %seq_len_hat=bin2dec(int2str(noisy_length'>0)); %%use this with variable rate packets
        seq_len_hat=seq_len; %%use this for fixed length packets

        %Here, we start the decoding process. noisy_symbols contain the sequence of
        %symbols that we recovered from the received frame. They are noisy, so the
        %first step involves mapping them into the most likely sequence of
        %constellation symbols.
        noisy_symbols=correction_factor/sqrt(ene)*rotation(1:seq_len_hat).*received_signal((signal_start_sample_hat):Fs/sym_rate:(signal_start_sample_hat+1)+(seq_len_hat-1)*Fs/sym_rate)/h_hat;

        %This next step maps the sequence of noisy symbols into the most likely
        %constellation symbols and then back to the associated sequence of bits.
        detected_bits=(char2psk(psk2char(noisy_symbols,con_size),2)+1)'/2;

        %Now that we have the sequence of received bits, we decode them using the
        %Turbo decoder. This removes the errors that can be correctable with the
        %code used.
        turbo_decode = comm.TurboDecoder('TrellisStructure', trellis,'InterleaverIndices',intrlvrIndices);
        decodedData = step(turbo_decode,detected_bits);

        %Here, we display the error rate, i.e., the mismatch between the original
        %data bits and the decoded bits. I also added raw_error_rate as the rate of
        %errors in the sequence of data bits before the Turbo decoder.
        raw_error_rate=sum(detected_bits~=encodedData)/length(detected_bits)
        ultimate_error_rate=sum(decodedData~=bits(1:length(decodedData)))/length(bits);

        %if psk2char(2*decodedData-1,2) == 'hello'
        %   audiowrite('../../noisy_key.wav', channel_out, Fs);
        %   break;
        %end

        if data_type=='T'||data_type=='t'
            disp(['Decoded text: ' psk2char(2*decodedData-1,2)]);
        end
    end
end