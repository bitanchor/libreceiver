'''
@blame R. Matt McCann <mccann.matt@gmail.com>
@brief A collection of helper functions for the build process
@copyright &copy; 2017 Evil Corp.
'''

import os
import sys
from datetime import datetime
from time import time

from glob import glob
from SCons.Script import Dir, GetOption, VariantDir
from subprocess import Popen

from misc.terminal import StdOut


# This is injected by the SCons build runner
env = None


def CleanCMake(target, source, env, build_dir='.', clear_log=True):
    '''
    Runs the cleanup of the cmake if needed.

    @param target Target to be cleaned
    @param source Directory where the Makefile lives
    @param env Environment build configuration
    '''
    CleanConfigureMake(target, source, env, do_uninstall=False, build_dir=build_dir)


def CleanConfigureMakeNoUninstall(target, source, env):
    CleanConfigureMake(target, source, env, do_uninstall=False)


def CleanConfigureMake(target, source, env, clear_log=True, do_uninstall=True, build_dir='.'):
    '''
    Runs the cleanup of the configure make if needed.

    @param target Target to be cleaned
    @param source Directory where the Makefile lives
    @param env Environment build configuration
    '''
    target_name = target[0]

    orig_dir = '.'
    base_dir = build_dir

    # If this is a cleanup command
    if GetOption('clean') and (len(sys.argv) == 2 or target_name in sys.argv):
        # Calculate the log file location
        log_file = '%s/logs/%s.clean' % (Dir('#').abspath, target_name)
        if clear_log:
            os.system('rm -f %s' % log_file)

        print StdOut.OKBLUE, "CLEANING...", StdOut.ENDC, target_name

        try:
            # If the build and install directories are empty
            if len(glob('./install/*')) == 0 and len(glob('%s/*' % base_dir)) == 0 \
                    and not os.path.exists('config.status'):
                # The directory is already clean, so nothing to do
                print StdOut.OKGREEN, "ALREADY CLEAN!", StdOut.ENDC, target_name

            # If the build and install directories are not empty
            else:
                # If we are uninstalling as part of the clean
                if do_uninstall:
                    # Run make uninstall
                    try:
                        run(orig_dir, base_dir, 'make uninstall', log_file)
                    except:
                        pass

                # Delete the contents of the install directory
                run(orig_dir, '.', 'rm install/* -rf', log_file)

                # Run make clean
                try:
                    run(orig_dir, base_dir, 'make clean', log_file)
                except:
                    pass

                # Delete the contents of the build directory
                if orig_dir != base_dir:
                    run(orig_dir, base_dir, 'rm * -rf', log_file)

                # If there is a config.status report
                if os.path.exists('config.status'):
                    # Delete the config.status report - we use this file as an indicator that the dependency
                    # has been built.
                    run(orig_dir, base_dir, 'rm config.status', log_file)

                # For each target
                for raw_target_lib in target[1:]:
                    target_lib = raw_target_lib[raw_target_lib.rfind("/")+1:]
                    path = Dir('#').abspath

                    if path.endswith(build_dir):
                        path += "/../install"
                    else:
                        path += "/install"

                    # If this target is a library
                    if 'lib' in target_lib:
                        # Delete the library link
                        run(orig_dir, base_dir, 'rm -f %s/lib/%s' % (path, target_lib), log_file)
                    # If this target is a program
                    else:
                        # Delete the program link
                        run(orig_dir, base_dir, 'rm -f %s/bin/%s' % (path, target_lib), log_file)

                print StdOut.OKGREEN, "CLEANED!   ", StdOut.ENDC, target_name

        except Exception:
            print StdOut.FAIL, "FAILED!    ", target_name, StdOut.ENDC
            print "\tSee %s for details" % log_file
            raise


def CMake(target, source, env, build_folder='build', custom_cmake_command=None):
    '''
    Uses cmake to build to the target.

    @param target Target program to build
    @param source Directory where the cmake lives
    @param env Environment build configuration
    '''
    orig_dir = Dir('#').abspath

    # Calculate the log file location
    target_name = target[0].get_abspath()
    target_name = target_name[target_name.rfind("/")+1:]
    log_file = '%s/logs/%s.build' % (Dir('#').abspath, target_name)

    # Remove the log file from the previous run
    os.system('rm -f %s' % log_file)

    # Let the user know we are starting the build process
    print StdOut.OKBLUE, "BUILDING... ", StdOut.ENDC, target_name

    # Calculate the absolute path of the source folder
    base_dir = source[0].get_abspath()
    base_dir = base_dir[:base_dir.rfind("/")]
    base_dir = '%s/%s' % (base_dir, build_folder)

    # Generate the Makefile
    cmake_command = 'cmake -DCMAKE_INSTALL_PREFIX:PATH=$PWD/install ..'
    if custom_cmake_command:
        cmake_command = custom_cmake_command
    run(orig_dir, base_dir, cmake_command, log_file)

    # Run the Makefile
    source[0] = source[0].File('%s/Makefile' % build_folder)
    ConfigureMake(
        target, source, env, build_dir='', clear_log=False, do_configure=False, do_install=True, show_building=False)

    return 0


def collect_sources(path):
    '''
    @params path Path to search
    @returns List of source files in the folder.
    '''
    c_sources = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*.c'))]
    cc_sources = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*.cc'))]
    cpp_sources = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*.cpp'))]
    assembly_sources = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*.s'))]

    return c_sources + cc_sources + cpp_sources + assembly_sources


def configure_emscripten(abs_path_to_emscription):
    ''' Configures the emscripten tool-chain. '''
    # Configure the required emscripten environment setup
    os.environ['PATH'] += ':%s' % abs_path_to_emscription
    os.environ['PATH'] += ':%s/clang/e1.37.21_64bit' % abs_path_to_emscription
    os.environ['PATH'] += ':%s/node/4.1.1_64bit/bin' % abs_path_to_emscription
    os.environ['PATH'] += ':%s/emscripten/1.37.21' % abs_path_to_emscription
    os.environ['EMSDK'] = abs_path_to_emscription
    os.environ['EM_CONFIG'] = '%s/.emscripten' % os.environ['HOME']
    os.environ['BINARYEN_ROOT'] = '%s/clang/e1.37.21_64bit/binaryen' % abs_path_to_emscription
    os.environ['EMSCRIPTEN'] = '%s/emscripten/1.37.21' % abs_path_to_emscription


def configure_scons_library(target, source, env, is_shared=False):
    ''' Configures the SCons Library target so it plays nice with our build system. '''
    target_base = target[:target.rfind('/')+1]
    target_name = target[target.rfind('/')+1:]

    # Configure the build directory
    build_dir = '#build/%s_lib' % target_name
    VariantDir(build_dir, '.')

    # Add the path prefix to the source files
    for iter in range(len(source)):
        source[iter] = build_dir + "/" + source[iter]

    # Set up the appropriately named target
    if is_shared:
        target_lib = env.SharedLibrary(target='%s/%s' % (target_base, target_name), source=source)
    else:
        target_lib = env.Library(target='%s/%s' % (target_base, target_name), source=source)

    env.Alias(target_name, target_lib)


def configure_scons_program(target, source, env):
    ''' Configures the SCons Program target so it plays nice with our build system. '''
    target_base = target[:target.rfind('/')+1]
    target_name = target[target.rfind('/')+1:]

    # Configure the build directory
    build_dir = '#build/%s_program' % target_name
    VariantDir(build_dir, '.')

    # Add the path prefix to the source files
    for iter in range(len(source)):
        source[iter] = build_dir + "/" + source[iter]

    # Set up the appropriately named target
    target_lib = env.Program(target='%s/%s' % (target_base, target_name), source=source)
    env.Alias(target_name, target_lib)

    return target_lib


def configure_scons_shared_library(target, source, env):
    ''' Configures the SCons SharedLibrary target. '''
    configure_scons_library(target, source, env, is_shared=True)


def get_environment():
    ''' @returns The appropriate environment. '''
    return env.Clone()


def MakeNoInstall(target, source, env, clear_log=True, show_building=True):
    ''' TODO '''
    ConfigureMake(target, source, env, clear_log, False, False, show_building)


def ConfigureMake(
        target, source, env, clear_log=True, show_building=True, do_configure=True, do_install=True,
        build_dir='build', install_dir='install', configure_command=None, make_command=None, install_command=None):
    '''
    Runs configure as necessary and then runs make.

    @param target Target program to build
    @param source Directory where the Makefile lives
    @param env Environment build configuration
    @param clear_log If True, deletes the log from the previous run
    @param show_building If True, shows the building progress messages
    @param build_dir Directory in which to build the object files (relative to the source folder)
    @param install_dir Directory in which to install the built targets (relative to the source folder)
    @param do_configure If True, runs configure before make
    @param do_install If True, runs make install after make
    @param custom_configure_command If set, runs this command as the configure step rather than the default
    @param custom_make_command If set, runs this command as the make step rather than the default
    @param custom_install_command If set, runs this command as the make install step rather than the default
    '''
    orig_dir = Dir('#').abspath

    # Calculate the absolute path of the source folder
    base_dir = source[0].get_abspath()
    base_dir = base_dir[:base_dir.rfind("/")]
    base_dir += '/' + build_dir

    # Calculate the log file location
    target_name = target[0].get_abspath()
    target_name = target_name[target_name.rfind("/")+1:]
    log_file = '%s/logs/%s.build' % (Dir('#').abspath, target_name)

    # Remove the log file from the last run
    if clear_log:
        os.system('rm -f %s' % log_file)

    # Tell the user we are starting the build
    if show_building:
        print StdOut.OKBLUE, "BUILDING... ", StdOut.ENDC, target_name, datetime.now()
    start_time = time()

    try:
        # If the software hasn't been configured yet and we are supposed to configure it
        if not os.path.exists('%s/config.status' % base_dir) and do_configure:
            # Select the command used to trigger the configure script
            final_configure_command = '../configure --prefix=$PWD/%s' % install_dir
            if configure_command:
                final_configure_command = configure_command

            # Run the configure command
            run(orig_dir, base_dir, final_configure_command, log_file)

        # Select the command used to trigger make
        final_make_command = 'make -j %d' % GetOption('num_jobs')
        if make_command:
            final_make_command = make_command

        # Run the make command
        run(orig_dir, base_dir, final_make_command, log_file)

        # If we are runnin a make install
        if do_install:
            # Select the command used to trigger make install
            final_install_command = 'make install -j %d' % GetOption('num_jobs')
            if install_command:
                final_install_command = install_command

            # Run the make install command
            run(orig_dir, base_dir, final_install_command, log_file)

        # Let the user know we've successfully built
        print StdOut.OKGREEN, "BUILT!      ", StdOut.ENDC, target_name, \
            "(%5.2ds)" % (time() - start_time)
    except Exception:
        # Let the user know we failed to build
        print StdOut.FAIL, "FAILED!     ", target_name, StdOut.ENDC
        print "\tSee %s for details" % log_file
        raise

    # Emit the symlinks for the produced targets
    for raw_target_lib in target[1:]:
        target_lib = raw_target_lib.get_abspath()
        target_lib = target_lib[target_lib.rfind("/")+1:]

        path = base_dir
        path += "/" + install_dir

        if 'lib' in target_lib:
            run(orig_dir, Dir('#').abspath + '/lib', 'ln -sf %s/lib/%s' % (path, target_lib), log_file)
        elif not target_lib.endswith('.h'):
            run(orig_dir, Dir('#').abspath + '/bin', 'ln -sf %s/bin/%s' % (path, target_lib), log_file)

    return 0


def run(orig_dir, target_dir, command, log_file=None):
    ''' Runs the provided command and pipes the output to log file. '''
    exit_code = None
    log = None

    if log_file:
        log = open(log_file, 'a')

    if log_file:
        log.write("======== Running ==========\n")
        log.write("%s\n" % command)
        log.write("===========================\n")
        log.flush()

        command = 'mkdir -p %s && cd %s && %s && cd %s' % (target_dir, target_dir, command, orig_dir)

        proc = Popen(command, stdout=log, stderr=log, shell=True)
        proc.communicate()
        exit_code = proc.wait()

    else:
        proc = Popen(command.split())
        proc.communicate()
        exit_code = proc.wait()

    if exit_code != 0:
        if log_file:
            log.write("======== Failed ==========\n")
            log.write('%s\n' % command)
            log.write("==========================\n\n")
            log.close()

        raise Exception(exit_code)

    if log_file:
        log.write('======== Done ==========\n')
        log.write('%s\n' % command)
        log.write('========================\n\n')
        log.close()

