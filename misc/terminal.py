'''
@blame R. Matt McCann <mccann.matt@gmail.com>
@brief Wraps stdout as well as providing various utilities
@copyright 2017 Evil Corp.
'''

import sys

from misc.py_extensions import classproperty


class StdErr(object):
    ''' Wraps stderr as well as providing various utilties. '''

    # This singleton instance
    _instance = None

    # True if StdErr has been written
    _is_written = False

    # The original stderr write
    _original_stderr_write = sys.stderr.write

    @classproperty
    def is_written(cls):
        ''' @returns True if written since last reset '''
        assert cls._instance, "You must first instantiate StdErr by calling StdErr.watch_stderr!"

        return cls._instance._is_written

    @classmethod
    def watch_stderr(cls):
        '''
        Replaces stdout with a watcher that indicates if stdout has been written to
        '''
        if cls._instance is None:
            cls._instance = StdErr()

        cls._instance._is_written = False
        sys.stderr = cls._instance

    def write(self, *a, **kw):
        self._is_written = True
        self._original_stderr_write(*a, **kw)


class StdOut(object):
    ''' Wraps stdout as well as providing various utities. '''

    # This singleton instance
    _instance = None

    # True if StdOut has been written
    _is_written = False

    # The original stdout
    _original_stdout_write = sys.stdout.write

    HEADER = '\033[95m'

    OKBLUE = '\033[94m'

    OKGREEN = '\033[92m'

    WARNING = '\033[93m'

    FAIL = '\033[91m'

    ENDC = '\033[0m'

    BOLD = '\033[1m'

    UNDERLINE = '\033[4m'

    CLEAR_LAST_LINE = '\033[F\033[2K\r\033[F'

    @classproperty
    def is_written(cls):
        ''' @returns True if written since last reset '''
        assert cls._instance, "You must first instantiate StdOut by calling StdOut.watch_stdout!"

        return cls._instance._is_written

    @classmethod
    def watch_stdout(cls):
        '''
        Replaces stdout with a watcher that indicates if stdout has been written to
        '''
        if cls._instance is None:
            cls._instance = StdOut()

        cls._instance._is_written = False
        sys.stdout = cls._instance

    def write(self, *a, **kw):
        self._is_written = True
        self._original_stdout_write(*a, **kw)

