#include "Beacon.h"

#include <stdexcept>
#include <sstream>

#include "constants.h"
#include "WavFile.h"
#include "polyfit.h"

using namespace evil_corp;
using namespace itpp;
using namespace std;

/** Interleaver indices used when generating the transmitter WAV file. */
static const char* DEFAULT_INTERLEAVER_INDICES =
    "18 11 16 39 29 32 19 37 14 5 26 20 17 4 7 15 12 40 3 10 9 6 1 38 21 34 24 36 13 31 "
    "8 25 22 35 33 23 2 27 30 28";

constexpr uint8_t Beacon::EXPECTED_DATA_PACKET[304];

constexpr uint8_t Beacon::EXPECTED_KEY[40];

vec normalizeAngle(vec a, double center) {
  return a - (2*pi * floor((a + pi - center) / (2 * pi)));
}

Beacon::Beacon() {
    throw std::runtime_error("Don't use the default constructor. It is only provided for boost::python");
}

Beacon::Beacon(const WavFile& audio_sample) :
    audio_sample_(audio_sample),
    data_max_length_(0),
    energy_per_symbol_(0),
    frequency_shift_(0),
    frequency_shift_last_(0),
    interleaver_indices_(DEFAULT_INTERLEAVER_INDICES),
    pilotStartSample(0),
    signalStartSample(0)
{ }

void Beacon::calc_derived_properties() {
    double fractional_symbol_rate = double(audio_sample_.sampling_frequency()) / SYMBOL_RATE;
    double integer_symbol_rate = audio_sample_.sampling_frequency() / SYMBOL_RATE;

    // Calculate the maximum data length in
    data_max_length_ = SYMBOL_RATE * audio_sample_.duration() - PILOT_LENGTH;

    // Calculate the energy allocated per symbol
    energy_per_symbol_ = TRANSMISSION_POWER / SYMBOL_RATE;

    // Check that the selected symbol rate is valid
    if (fractional_symbol_rate != integer_symbol_rate) {
        stringstream error_message;

        error_message << "Audio sampling frequency (" << audio_sample_.sampling_frequency() << " Hz) / symbol rate ("
            << SYMBOL_RATE << ") must be round integer, but got " << fractional_symbol_rate;
        throw std::invalid_argument(error_message.str());
    }

    // Check that the maximum data length is a valid positive value
    if (data_max_length_ <= 0) {
        stringstream error_message;

        error_message << "Expected max data length to be a positive value, but got " << data_max_length_
            << ". (symbol_rate=" << SYMBOL_RATE << ", duration=" << audio_sample_.duration() << ", pilot_length="
            << PILOT_LENGTH << ")";
        throw std::invalid_argument(error_message.str());
    }
}

Beacon Beacon::create(const string& audio_sample_file) {
    int last_pilot_start_sample = -1;
    WavFile wav = WavFile::create(audio_sample_file);

    Beacon beacon(wav);

    // Calculate the time of each sample within the audio signal
    beacon.calc_time();

    // Calculate the derived signal properties
    beacon.calc_derived_properties();

    beacon.initialize();

    // Keep factoring out calculate frequency mismatch until we settle on a final pilot start position
    size_t iterations = 0;
    while ((last_pilot_start_sample != beacon.pilotStartSample) && (iterations++ < 10)) {
        // Update the last pilot start so we can use for loop evaluation
        last_pilot_start_sample = beacon.pilotStartSample;

        // TODO(mmccann) - Clean this up
        beacon.initializeCarrier();
        beacon.demodSignal = beacon.Demod();
        beacon.receivedSignal = beacon.matchedFilter();
        beacon.synchronization();
        beacon.calculateShifts();
    }

    beacon.correction = beacon.correctionFactor();
    beacon.rot = beacon.rotationFactor();
    beacon.noisy_symbols_ = beacon.noisySymbols();

    // Translate the noisy symbols into a binary data packet
    beacon.translate_noisy_symbols_into_data_packet();

    // Calculate the error rate of the data packet
    beacon.calc_hamming_distance();

    // Decode the data packet to extract the transmitted key
    beacon.decode_data_packet();

    // Calculate the error rate of the decoded key
    beacon.calc_final_hamming_distance();

    return beacon;
}

void Beacon::initialize() {
    pilot = pilots;
    pulse = pulseSymbols;
}

void Beacon::calc_time() {
    // One time value for each sample in the audio
    time_.set_size(audio_sample_.samples().size());

    // Calculate the times
    for (size_t iter = 0; iter < audio_sample_.samples().size(); iter++) {
        time_[iter] = static_cast<double>(iter) / audio_sample_.sampling_frequency();
    }
}

#ifdef ENABLE_PY_BINDINGS
#include <boost/python.hpp>

void Beacon::export_to_python() {
    boost::python::class_<Beacon>(
            "Beacon", boost::python::init<>()
        )
        .def("create", &Beacon::create)
        .def("hamming_distance", &Beacon::hamming_distance)
        .def("hamming_distance_key", &Beacon::hamming_distance_key)
        .def("is_in_location", &Beacon::is_in_location)
        .staticmethod("create");
}
#endif

/**
 * NOTE(mmccann) -
 *
 * These thresholds are calculated assuming that noisy has an even fifty/fifty likelihood of being 0 or 1. The
 * probability distribution for the hamming distances is calculated, and a range of out-of-context hamming distances
 * is selected such that the confidence of an in-context signal is 99.7%
 *
 * The utils/calc_hamming_distance_prob_distribution.py script automates these calculations.
 */
bool Beacon::is_in_location() const {
    const float out_of_location_min = 0.3684;
    const float out_of_location_max = 0.6053;

    bool is_in_location = (hamming_distance_ < out_of_location_min) || (hamming_distance_ > out_of_location_max);

    if (is_in_location) return is_in_location;

    return (hamming_distance_key_ <= 0.25) || (hamming_distance_key_ >= 0.75);
}

void Beacon::calc_hamming_distance() {
    double distance = 0;

    // For each bit in the data packet
    for (size_t iter = 0; iter < data_packet_.size(); iter++) {
        // If there are more data packet bits than expected
        if (iter >= sizeof(EXPECTED_DATA_PACKET)) {
            // Mark this bit as incorrect
            distance += 1;
        } else {
            distance += abs(data_packet_[iter] - EXPECTED_DATA_PACKET[iter]);
        }
    }

    // For each bit that the data packet is shorted than the expected data packet
    for (size_t iter = data_packet_.size(); iter < sizeof(EXPECTED_DATA_PACKET); iter++) {
        // Mark this missing bit as incorrect
        distance += 1;
    }

    // Scale the distance by the length of the expected data packet
    hamming_distance_ = distance / sizeof(EXPECTED_DATA_PACKET);
}

void Beacon::calc_final_hamming_distance() {
    double distance = 0;

    // For each bit in the decoded key
    for (size_t iter = 0; iter < decoded_key_.size(); iter++) {
        // If there are more decoded key bits than expected
        if (iter >= sizeof(EXPECTED_KEY)) {
            // Mark this bit as incorrect
            distance += 1;
        } else {
            distance += abs(decoded_key_[iter] - EXPECTED_KEY[iter]);
        }
    }

    // For each bit that the decoded key is shorted than the expected key
    for (size_t iter = decoded_key_.size(); iter < sizeof(EXPECTED_KEY); iter++) {
        // Mark this missing bit as incorrect
        distance += 1;
    }

    // Scale the distance by the length of the expected key
    hamming_distance_key_ = distance / sizeof(EXPECTED_KEY);
}

/** Initialzing the Receive Carrier
 * 	Matlab Equivalent : rec_carrier=2*exp(-j*2*pi*(fc+f0)*(t-t0))
**/
void Beacon::initializeCarrier() {
	std::complex<double>i(0,-1);
    long double factor = 2 * pi;
                factor *= static_cast<long double>(CARRIER_FREQUENCY) - frequency_shift_;
    vec curious = static_cast<double>(factor) * time_;

	receiveCarrier =  2 * exp(i * 2 * pi * (double(CARRIER_FREQUENCY) - frequency_shift_) * time_);
}

void Beacon::translate_noisy_symbols_into_data_packet() {
    // Resize the data packet to contain the translated noisy symbols
    data_packet_.resize(noisy_symbols_.size());

    // For each noisy symbol
    for (size_t iter = 0; iter < noisy_symbols_.size(); iter++) {
        // This loop is equivalent to :
        //     detected_bits=(char2psk(psk2char(noisy_symbols,con_size),2)+1)'/2;
        // with:
        //     psk2char(y,2) = (sign(real(y))+1)/2;
        double complex_magnitude = std::abs(noisy_symbols_[iter]);
        double psk2char = (noisy_symbols_[iter].real() / complex_magnitude + 1) / 2;

        // Calculate the bit equivalent for this noisy symbol
        data_packet_[iter] = std::round(psk2char);
    }
}

/** Beacon to get the baseband signal
*   Matlab Equivalent : demod_signal=rec_carrier.*channel_out
**/
cvec Beacon::Demod() {
    cvec demod = elem_mult(receiveCarrier, audio_sample_.samples_as_itpp_cvec());

    return demod;
}

cvec Beacon::matchedFilter() {
    int N = pulse.length() + demodSignal.length() - 1;
    cvec return_value = ifft(elem_mult(fft(pulse, N),fft(demodSignal,N)));

    return return_value;

}

/** Synchronization (Pilot Aided Sync - Correlate pilot with demodulated signal)
* 	MATLAB Equivalent:
* 	h_hat=0;
* 	for k=(pulse_len+1)/2 : length(t)-(pilot_len*Fs/sym_rate+(pulse_len+1)/2) %potential center samples of any symbol
*    	potential_pilot_samples = k : Fs/sym_rate : k+(pilot_len-1)*Fs/sym_rate;
*    	h_cur=conj(pilots)*received_signal(potential_pilot_samples)/sqrt(ene)/pilot_len;
*    	if abs(h_cur)>abs(h_hat),  %in real time operation, change this condition to a threshold
*        	h_hat=h_cur;
*        	pilot_hat=received_signal(potential_pilot_samples)/sqrt(ene);
*        	pilot_start_sample_hat=k;
*        	signal_start_sample_hat=k+pilot_len*Fs/sym_rate;
*    	end
* 	end
**/
void Beacon::synchronization() {
    complex<double> hHatValue;
    complex<double> hCurValue;
    size_t iter_start;
    size_t iter_stop;

    // Zero out any intermediary values collected with a previous pass
    hHat = "(0,0)";
    hHatMat = "(0,0)";

    iter_start = PULSE_LENGTH / 2;
    iter_stop = time_.length() - (PILOT_LENGTH + DATA_PACKET_LENGTH) * audio_sample_.sampling_frequency() / SYMBOL_RATE;

    for (size_t k = iter_start; k < iter_stop; k++) {
        string sa = to_string(k) + ":" + to_string(int(audio_sample_.sampling_frequency()/SYMBOL_RATE)) + ":" + to_string(int(k + (PILOT_LENGTH)*audio_sample_.sampling_frequency()/SYMBOL_RATE)-1);
        potential_pilot_samples = sa;

        cmat x = receivedSignal(potential_pilot_samples)/std::sqrt(energy_per_symbol_)/PILOT_LENGTH;
        cmat y = conj(pilot).transpose();

        hCurMat = y*x;

        mat abs_hCur = abs(hCurMat);
        mat abs_hHat = abs(hHatMat);
        hCurValue = abs_hCur(0,0);

        if (abs_hCur.cols() && abs_hCur(0,0) > abs_hHat(0,0)) {

            hHatMat = hCurMat;
            hHatValue = hHatMat(0,0);
            pilotHat = receivedSignal(potential_pilot_samples) / std::sqrt(energy_per_symbol_);
            pilotStartSample = k;
            signalStartSample = k + (PILOT_LENGTH) * audio_sample_.sampling_frequency()/SYMBOL_RATE ;
        }
    }
}

/** Calculate Frequency and Phase Shifts
*   MATLAB Equivalent:
*   shifts=polyfit(0:2*pi/sym_rate:(pilot_len-1)*2*pi/sym_rate,unwrap(angle(pilots./pilot_hat.')),1);
*	freq_shift_hat=shifts(1)
*	phase_shift_hat=shifts(2)
*
*	This function is yet to be complete (To find polyfit fn=unction in c++)
**/
void Beacon::calculateShifts() {
    string polyfitVector = to_string(0) + ":" + to_string(2*pi/SYMBOL_RATE) + ":" + to_string((PILOT_LENGTH)*2*pi/SYMBOL_RATE);
	vec myPoly(PILOT_LENGTH);
    for (size_t iter = 0; iter < PILOT_LENGTH; iter++) {
        myPoly[iter] = iter * 2 * pi / SYMBOL_RATE;
    }

    vec y = angle(elem_div(pilot, pilotHat));
    vec y1 = normalizeAngle(y, 0.0);

    const unsigned int order = 1;
    const unsigned int countOfElements = 100;
    int result;
    double coefficients[order + 1]; // resulting array of coefs

    result = polyfit(myPoly._data(),
                   y1._data(),
                   countOfElements,
                   order,
                   coefficients);

    frequency_shift_last_ = coefficients[1];
	frequency_shift_ += coefficients[1];
}


/** Calculate the Correction Factor to be applied to Symbols
*	MATLAB Equivalent:
*	correction_factor=1/pilot_len*(1+2*sum(cos(pi*freq_shift_hat/sym_rate*(pilot_len-2*(1:floor((pilot_len-1)/2))))))
*
**/
double Beacon::correctionFactor() {
	string x = to_string(1) + ":" + to_string((PILOT_LENGTH-1)/2);
	vec intrMd = x;
	double correction = (1.0/PILOT_LENGTH)*(1.0 + 2.0*sum(cos(pi * (frequency_shift_/SYMBOL_RATE) * (PILOT_LENGTH - (2.0*intrMd)))));
    return correction;
}

/** Calculate the Rotation Factor to be applied to Symbols
*	MATLAB Equivalent:
*	OLD ---> rotation=exp(j*2*pi*freq_shift_hat*((0:1/sym_rate:(max_len+pilot_len+8)/sym_rate))).'
*   rotation=exp(j*2*pi*shifts(1)*((0:1/sym_rate:(seq_len-1)/sym_rate))).';
*
**/
cvec Beacon::rotationFactor() {
	vec exp1(DATA_PACKET_LENGTH + PILOT_LENGTH);
    for (int iter = 0; iter < DATA_PACKET_LENGTH + PILOT_LENGTH; iter++) {
        exp1[iter] = double(iter) / SYMBOL_RATE;
    }

	std::complex<double>i(0,1);
	cvec rotation =  exp(i * 2 * pi * frequency_shift_last_ * exp1);

    return rotation;
}

/** Calculate the Length of the noisy Symbols
*	MATLAB Equivalent:
*	noisy_length=correction_factor/sqrt(ene)*rotation(1:8).*received_signal(signal_start_sample_hat:Fs/sym_rate:signal_start_sample_hat+(8-1)*Fs/sym_rate)/h_hat;
*	seq_len_hat=bin2dec(int2str(uintnoisy_length'>0));
*
**/
double Beacon::noisyLength() {
	string x = to_string(signalStartSample) + ":" + to_string(audio_sample_.sampling_frequency()/SYMBOL_RATE) + ":" + to_string(signalStartSample + (8-1)*audio_sample_.sampling_frequency()/SYMBOL_RATE);
	ivec len = "0 1 2 3 4 5 6 7";
	/** Add divide by hHat **/
	cvec noisyLen = correction / std::sqrt(energy_per_symbol_) * elem_mult(rot(len), receivedSignal(len));
	vec dummy(8);
	dummy = real(noisyLen);
	for (int i = 0; i < noisyLen.length(); i++){
		if(dummy(i) > 0){
			dummy(i) = 1;
		}
		else{
			dummy(i) = 0;
		}
	}
	bool msb_first = true;
	return bin2dec(to_bvec(dummy), msb_first);
}

/** Estimate the noisy Symbols
*	MATLAB Equivalent:container
*	noisy_symbols=correction_factor/sqrt(ene)*rotation(9:seq_len_hat+8).*received_signal((signal_start_sample_hat+8*Fs/sym_rate):Fs/sym_rate:(signal_start_sample_hat+8*Fs/sym_rate)+(seq_len_hat-1)*Fs/sym_rate)/h_hat;
*
**/
cvec Beacon::noisySymbols() {
	//string x = to_string(signalStartSample+8*Fs/SYMBOL_RATE) + ":" + to_string(Fs/SYMBOL_RATE) + ":" + to_string((signalStartSample+8*Fs/SYMBOL_RATE) + (seqLenHat-1)*Fs/SYMBOL_RATE);
    string x = to_string(signalStartSample) + ":" + to_string(double(audio_sample_.sampling_frequency())/SYMBOL_RATE) + ":" + to_string((signalStartSample) + (DATA_PACKET_LENGTH-1)*double(audio_sample_.sampling_frequency())/SYMBOL_RATE);
	//ivec len = "0 1 2 3 4 5 6 7";
	ivec len = x;
    //return correction / std::sqrt(energy_per_symbol_) * receivedSignal / hHatMat(0,0);
	cvec noisy_symbols = correction / std::sqrt(energy_per_symbol_) * elem_mult(rot(0, DATA_PACKET_LENGTH-1), receivedSignal(len)) / hHatMat(0,0);
    return noisy_symbols;
}

void Beacon::decode_data_packet() {
    const int constraint_length = 4;
    vec data_packet(data_packet_.size());
    bvec decoded_data(sizeof(EXPECTED_KEY));
	  Turbo_Codec decoder;
    bvec expected_key(sizeof(EXPECTED_KEY));

    // It's a requirement to declare the generator polynomial like this
    // ivec generator_polynomial = "13 15 11 17" won't work
    ivec generator_polynomial(4);
    generator_polynomial(0) = 013;
    generator_polynomial(1) = 015;
    generator_polynomial(2) = 011;
    generator_polynomial(3) = 017;

    // Pack the received data packet into an ITPP friendly container
    for (size_t iter = 0; iter < data_packet_.size(); iter++) {
        if (data_packet_[iter] == 0) {
        data_packet[iter] = 1;
        } else {
        data_packet[iter] = -1;
        }
    }

    // Pack the expected key into an ITPP friendly container
    for (size_t iter = 0; iter < sizeof(EXPECTED_KEY); iter++) {
        expected_key[iter] = EXPECTED_KEY[iter];
    }

    // Configure the decoder
    decoder.set_parameters(generator_polynomial, generator_polynomial, constraint_length, interleaver_indices_, 6, "MAP", 1.0, false);

    // Decode the message
	decoder.decode(data_packet, decoded_data, expected_key);

    //Pack the decoded bits into a human readable string
    for (size_t iter = 0; iter < decoded_data.size() / 8; iter++) {
        uint8_t character = 0;

        for (size_t bit_iter = 0; bit_iter < 8; bit_iter++) {
            decoded_key_.push_back(int(decoded_data[iter * 8 + bit_iter]));
            character |= int(decoded_data[iter * 8 + bit_iter]) << (8 - bit_iter - 1);
        }
        key_ += character;
    }
}
