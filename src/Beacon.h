#include <stdio.h>
#include <itpp/itbase.h>
#include <itpp/base/vec.h>
#include <itpp/base/converters.h>
#include <itpp/base/math/elem_math.h>
#include <itpp/base/matfunc.h>
#include <itpp/base/specmat.h>
#include <itpp/base/math/min_max.h>
#include <itpp/signal/transforms.h>
#include <itpp/signal/poly.h>
#include <itpp/base/math/elem_math.h>
#include <itpp/itexports.h>
#include <itpp/comm/turbo.h>
#include <string>

#include "WavFile.h"

using namespace std;
using namespace itpp;


namespace evil_corp {

class Beacon {
public:
    /** Default frequency used to encode the signal data. */
    static const uint32_t CARRIER_FREQUENCY = 19000;

    /** Length of the data packet comprising the Beacon data. */
    static const uint32_t DATA_PACKET_LENGTH = 304;

    /** The expected data packet contents when a Beacon signal is properly received. */
    // static constexpr uint8_t EXPECTED_DATA_PACKET[304] = {
    //     0,0,0,0,1,1,0,1,1,1,0,0,0,1,1,0,0,1,1,0,0,0,0,1,0,
    //     0,1,0,1,1,0,1,0,0,0,0,0,1,1,1,0,1,0,1,0,0,0,0,0,0,
    //     0,0,1,0,1,1,0,1,1,1,1,1,1,1,0,1,1,1,1,0,1,1,1,1,0,
    //     0,1,0,0,0,0,0,1,0,0,0,0,0,0,1,1,1,1,1,0,1,0,0,0,1,
    //     1,1,0,0,1,1,0,1,1,1,1,1,0,0,0,1,1,0,1,1,0,0,1,0,0,
    //     0,1,1,0,0,0,1,1,0,1,0,0,1,1,1,1,1,1,1,0,0,0,1,1,1,
    //     0,0,0,0,0,1,1,1,0,0,0,0,1,0,1,1,1,0,0,1,1,0,0,0,1,
    //     1,1,0,0,1,0,0,1,0,1,0,1,0,0,0,1,1,0,0,0,1,1,1,0,0,
    //     1,1,1,1,0,1,0,1,0,1,0,1,1,0,1,1,0,0,0,1,0,0,1,0,0,
    //     0,1,1,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,1,1,0,1,0,1,1,
    //     0,0,1,0,0,0,1,1,1,1,0,1,1,1,1,0,1,1,1,1,0,0,1,1,1,
    //     1,0,0,1,0,0,1,1,1,1,0,1,1,1,1,1,1,0,0,1,1,0,1,0,0,
    //     1,1,1,1
    // };

    static constexpr uint8_t EXPECTED_DATA_PACKET[304] = {
        0,0,0,0,1,1,1,1,1,1,1,1,0,1,1,0,1,0,1,1,0,0,0,1,1,
        0,1,0,1,1,0,0,0,0,1,0,0,1,1,0,0,0,0,1,1,0,1,0,1,0,
        0,0,0,0,0,1,0,1,0,1,0,0,0,1,0,0,1,1,0,1,1,1,1,1,0,
        0,1,0,0,0,0,1,1,1,0,0,0,0,1,1,1,1,1,1,1,0,1,0,0,1,
        0,1,0,1,1,1,0,0,1,1,0,0,0,0,0,0,0,1,1,1,0,1,0,0,0,
        0,1,1,0,0,0,0,0,0,1,1,0,1,1,1,1,1,1,1,0,1,0,1,1,1,
        1,0,1,1,0,1,0,1,1,0,0,0,1,1,0,1,0,0,0,1,0,1,1,0,0,
        1,1,0,0,1,0,0,1,0,0,1,0,1,1,0,1,0,1,0,0,1,1,1,0,0,
        0,1,0,1,0,0,1,1,1,0,0,1,0,1,1,0,1,0,0,1,1,1,0,0,0,
        0,1,1,1,1,0,1,0,0,1,0,0,0,1,0,1,0,0,1,0,0,1,1,0,0,
        1,1,1,0,1,0,1,0,0,1,0,0,1,0,1,1,1,1,1,1,1,1,0,1,1,
        1,1,1,1,1,0,1,0,1,1,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,
        0,0,0,0
    };

    /** The expected key message when a Beacon signal is properly decoded. */
    static constexpr uint8_t EXPECTED_KEY[40] = {
        0,1,1,0,1,0,0,0,0,1,1,0,0,1,0,1,0,1,1,0,1,1,0,0,0,
        1,1,0,1,1,0,0,0,1,1,0,1,1,1,1
    };

    /** The maximum hamming distance accepted as in-location. */
    static constexpr double IN_LOCATION_ERROR_THRESHOLD = 0.15;

    /** Default number of symbols in the pilot signal. */
    static const uint16_t PILOT_LENGTH = 101;

    /** Default number of samples per pulse. */
    static const uint16_t PULSE_LENGTH = 2049;

    /** Default number of symbols encoded per second. */
    static const uint16_t SYMBOL_RATE = 724;

    // HACK
    Beacon();

    // TODO(mmccann) - Yuck!
    int pilot_length, pilotStartSample, signalStartSample;
    double correction;
    vec olyFit;
    vec encodedData;
    ivec potential_pilot_samples;
    cvec pilotHat;
    cvec pilot, receivedSignal, receiveCarrier, pulse, hHat, hCur, rot;
    cvec demodSignal;
    cmat hCurMat, hHatMat;

    /**
     * Factory method that creates a Beacon from the provided audio sample.
     *
     * @param audio_sample_file (in) File path of a mono 16-bit LSB PCM encoded WAV file containing the audio sample
     *     to be parsed.
     * @returns The Beacon parsed from the audio sample
     */
    static Beacon create(const std::string& audio_sample_file);

        void initialize();
        void synchronization();
		void initializeCarrier();
		cvec Demod();
		cvec matchedFilter();
		void calculateShifts();
		double correctionFactor();
		cvec rotationFactor();
		double noisyLength();
		cvec noisySymbols();

    /** Calculates the derived signal properties. */
    void calc_derived_properties();

    /** Calculates the hamming distance between the data packet and the expected data packet. */
    void calc_hamming_distance();

    /** Calculates the hamming distance between the decoded key and the expected key. */
    void calc_final_hamming_distance();

    /** Calculates the time of each sample within the audio signal. */
    void calc_time();

    /** Exposes this class to Pythonland. */
    static void export_to_python();

    /** @returns True if the beacon indicates the machine is within the location context. */
    bool is_in_location() const;

    /** Translates the noisy symbols into binary data. */
    void translate_noisy_symbols_into_data_packet();

    /*******************************************************************************************************************
     * Property accessors
     ******************************************************************************************************************/
    const std::vector<uint8_t>& data_packet() const { return data_packet_; }

    void data_packet(const std::vector<uint8_t>& data_packet) { data_packet_ = data_packet; }

    const std::vector<uint8_t>& decoded_key() const { return decoded_key_; }

    void decoded_key(const std::vector<uint8_t>& decoded_key) { decoded_key_ = decoded_key; }

    double frequency_shift() const { return frequency_shift_; }

    double hamming_distance() const { return hamming_distance_; }

    double hamming_distance_key() const { return hamming_distance_key_; }

    const std::string& key() const { return key_; }

    const itpp::cvec& noisy_symbols() const { return noisy_symbols_; }

    const itpp::vec& time() const { return time_; }

private:
    /** Decodes the received data packet into the transmitted key. */
    void decode_data_packet();

    /** Total power (???) of the signal transmission. */
    static constexpr double TRANSMISSION_POWER = 200;

    /**
     * Constructor; Must use factory method to create Beacon.
     *
     * @param audio_sample Audio sample from which the Beacon is parsed
     */
    Beacon(const WavFile& audio_sample);

    /** Audio sample from which the beacon is parsed. */
    WavFile audio_sample_;

    /** Maximum number of data symbols that can be encoded in a packet. */
    uint16_t data_max_length_;

    /** Data packet extracted from this beacon's audio sample. */
    std::vector<uint8_t> data_packet_;

    /** Key decoded from the data packet using turbo decoder. */
    std::vector<uint8_t> decoded_key_;

    /** Energy allocated per symbol. */
    double energy_per_symbol_;

    /** Frequency shift required to parse the audio signal. */
    double frequency_shift_;

    /** Last frequency shift calculated when evaluating the audio signal. */
    double frequency_shift_last_;

    /** Measure of error within the beacon's data packet versus the expected data packet. */
    double hamming_distance_;

    /** Measure of error within the beacon's decoded key versus the expected key. */
    double hamming_distance_key_;

    /** Interleaver indices used in the turbo decode process. */
    itpp::ivec interleaver_indices_;

    /** Key decoded from the received data packet. */
    std::string key_;

    /** Noisy symbol data extracted from the audio signal. */
    itpp::cvec noisy_symbols_;

    /** Time of each sample within the audio signal. */
    itpp::vec time_;
}; // class Demodulation

}; // namespace evil_corp
