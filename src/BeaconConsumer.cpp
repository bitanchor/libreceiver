/***********************************************************************************************************************
 * @blame R. Matt McCann <mccann.matt@gmail.com>
 * @brief Implementation for the BeaconConsumer class.
 * @copyright &copy; 2017 Evil Corp.
 **********************************************************************************************************************/

#include "BeaconConsumer.h"


using namespace evil_corp;
using namespace ipc;
using namespace std;


BeaconConsumer::BeaconConsumer() :
    is_sample0_next_(true),
    sample0_file_path_("/tmp/human_proof.0.wav"),
    sample0_lock_name_("human_proof__audio_sample0_lock"),
    sample1_file_path_("/tmp/human_proof.1.wav"),
    sample1_lock_name_("human_proof__audio_sample1_lock")
{ }

/***********************************************************************************************************************
 * Implementation Note(mmccann):
 *
 * I'm keeping the creation of the mutex's as a separate function, rather than having them instantiated by default in
 * the constructor, because they have system-level side-effects (i.e. the creation of the named mutexes).
 *
 * If the caller wants to change the mutex names, it's preferable that the constructor not create unused/unexpected
 * system side-effects for named mutexes that are being overridden anyways.
 **********************************************************************************************************************/
void BeaconConsumer::init() {
    // Create the sample locks
    sample0_lock_ = make_shared<named_mutex>(open_or_create_t(), sample0_lock_name_.c_str());
    sample1_lock_ = make_shared<named_mutex>(open_or_create_t(), sample1_lock_name_.c_str());

    // Lock the sample locks as no samples should be available yet
    sample0_lock_->try_lock();
    sample1_lock_->try_lock();
}

bool BeaconConsumer::is_sample0_next() const {
    return is_sample0_next_;
}

void BeaconConsumer::sample0_file_path(const std::string& sample0_file_path) { sample0_file_path_ = sample0_file_path; }

void BeaconConsumer::sample0_lock_name(const std::string& sample0_lock_name) { sample0_lock_name_ = sample0_lock_name; }

void BeaconConsumer::sample1_file_path(const std::string& sample1_file_path) { sample1_file_path_ = sample1_file_path; }

void BeaconConsumer::sample1_lock_name(const std::string& sample1_lock_name) { sample1_lock_name_ = sample1_lock_name; }