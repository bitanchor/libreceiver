/**
 * @blame R. Matt McCann <mccann.matt@gmail.com>
 * @brief Parent class for the BeaconParser and BeaconListener that holds their shared details for communication.
 * @copyright &copy; 2017 Evil Corp.
 */

#ifndef BEACON_CONSUMER_H_
#define BEACON_CONSUMER_H_

#include <boost/interprocess/sync/named_mutex.hpp>
#include <memory>
#include <string>

namespace ipc = boost::interprocess;

namespace evil_corp {

/**
 * Parent class for the BeaconParser and BeaconListener classes that holds the details of their shared communications.
 */
class BeaconConsumer {
public:
    BeaconConsumer();

    /**
     * Initializes the state of the BeaconConsumer.
     *
     * Note: This function produces a system-wide side-effect when creating the mutexes guarding the audio samples.
     */
    void init();

    bool is_sample0_next() const;

    void sample0_file_path(const std::string& sample0_file_path);

    void sample0_lock_name(const std::string& sample0_lock_name);

    void sample1_file_path(const std::string& sample1_file_path);

    void sample1_lock_name(const std::string& sample1_lock_name);

protected:
    /** True if the next sample to be recorded is sample0. */
    bool is_sample0_next_;

    /** File path of the first audio sample's WAV file. */
    std::string sample0_file_path_;

    /** The mutex guarding read access to the first audio sample. */
    std::shared_ptr<ipc::named_mutex> sample0_lock_;

    /** The name of the lock used to guard the first audio sample. */
    std::string sample0_lock_name_;

    /** File path of the second audio sample's WAV file. */
    std::string sample1_file_path_;

    /** The mutex guarding read access to the second audio sample. */
    std::shared_ptr<ipc::named_mutex> sample1_lock_;

    /** The name of the lock used to guard the second audio sample. */
    std::string sample1_lock_name_;

}; // class BeaconConsumer

}; // namespace evil_corp

#endif // #ifndef BEACON_CONSUMER_H_