#include "BeaconListener.h"

#include <boost/filesystem.hpp>
#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <sstream>


using namespace boost::filesystem;
using namespace evil_corp;
using namespace ipc;
using namespace std;


BeaconListener::BeaconListener() :
    BeaconConsumer(),
    sampling_duration_(DEFAULT_SAMPLING_DURATION),
    sampling_rate_(DEFAULT_SAMPLING_RATE)
{ }


void BeaconListener::collect_next_sample() {
    stringstream record_command;
    int return_code;
    stringstream temp_sample_file;

    // Check that the BeaconListener was initialized before starting sample collection
    if (!sample0_lock_) {
        throw std::runtime_error("You must call init() before calling collect_next_sample()");
    }

    // Build the file name for the temp location of the audio sample
    if (is_sample0_next_) {
        temp_sample_file << sample0_file_path_ << ".tmp.wav";
    } else {
        temp_sample_file << sample1_file_path_ << ".tmp.wav";
    }

    // Build the record command
    // TODO(mmccann) - Yikes, this is a hacky, non-portable solution!
    record_command << "arecord -r " << sampling_rate_ << " -f S16_LE -d "
                   << int(sampling_duration_) << " " << temp_sample_file.str() << " 2> /dev/null";

    // Record the audio sample
    return_code = system(record_command.str().c_str());

    // If we failed to record the audio sample
    if (return_code != 0) {
        stringstream error_message;

        // The arecord command doesn't provide documented error feedback, so let's just bug out
        error_message << "Recording the audio sample failed with the record code " << return_code;
        throw runtime_error(error_message.str());
    }

    // Lock the next sample's lock
    if (is_sample0_next_) {
        sample0_lock_->try_lock();
    } else {
        sample1_lock_->try_lock();
    }

    // Move the audio sample from its temp location to it's intended destination for eventual consumption
    if (is_sample0_next_) {
        rename(temp_sample_file.str(), sample0_file_path_);
    } else {
        rename(temp_sample_file.str(), sample1_file_path_);
    }

    // Unlock the next sample's lock to indicate it's ready for consumption
    if (is_sample0_next_) {
        sample0_lock_->unlock();
    } else {
        sample1_lock_->unlock();
    }

    // Toggle which lock is next
    is_sample0_next_ = !is_sample0_next_;
}

void BeaconListener::run() {
    BeaconListener listener;

    // Initialize the listener
    listener.init();

    // Keep collecting samples until the end of time
    while (true) {
        listener.collect_next_sample();
    }
}

void BeaconListener::sampling_duration(const uint8_t sampling_duration) {
    sampling_duration_ = sampling_duration;
}