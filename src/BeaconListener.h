/**
 * @blame R. Matt McCann <mccann.matt@gmail.com>
 * @brief This class continually collects audio samples and stashes them for eventual parsing by the BeaconParser.
 * @copyright &copy; 2017 Evil Corp.
 */

#ifndef BEACON_LISTENER_H_
#define BEACON_LISTENER_H_

#include <boost/interprocess/sync/named_mutex.hpp>
#include <memory>
#include <string>

#include "BeaconConsumer.h"

namespace ipc = boost::interprocess;

namespace evil_corp {

/**
 * The class continually collects audio samples and stashes them for eventual parsing by the BeaconParser.
 *
 * The listener switches between two audio recording files, with each file guarded by a mutex. When the mutex is
 * unlocked, the audio file is ready for parsing.
 */
class BeaconListener : public BeaconConsumer {
public:
    /** Default sampling duration (in seconds) used when recording the audio samples. */
    static const uint8_t DEFAULT_SAMPLING_DURATION = 3;

    /** Default sampling rate (in Hz) used when recording the audio samples. */
    static const uint32_t DEFAULT_SAMPLING_RATE = 41992;

    BeaconListener();

    /** Creates a BeaconListener and then continually runs it. */
    static void run();

    /** Collects the next audio sample and updates the sample locks accordingly. */
    void collect_next_sample();

    void sampling_duration(const uint8_t sampling_duration);

    inline uint16_t sampling_rate() const { return sampling_rate_; }

private:
    /** Sampling duration (in seconds) used when recording the audio samples. */
    uint8_t sampling_duration_;

    /** Sampling rate (in Hz) used when creating the audio samples. */
    uint32_t sampling_rate_;

}; // class BeaconListener

}; // namespace evil_corp

#endif // #ifndef BEACON_LISTENER_H_