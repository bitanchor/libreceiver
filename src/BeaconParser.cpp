/**
 * @blame R. Matt McCann <mccann.matt@gmail.com>
 * @brief Implementation of the BeaconParser class
 * @copyright &copy; 2017 Evil Corp.
 */

#include "BeaconParser.h"

#include <cstdio>

#include "Beacon.h"

using namespace boost::posix_time;
using namespace evil_corp;

BeaconParser::BeaconParser() :
    BeaconConsumer(),
    location_context_lock_name_("/human_proof__in_location"),
    no_beacon_threshold_(milliseconds(DEFAULT_NO_BEACON_THRESHOLD)),
    when_last_beacon_seen_(second_clock::local_time())
{ }

ptime BeaconParser::calc_lock_wait_deadline() {
    time_duration wait_time = seconds(0);

    // If there is time left before the deadline
    if (no_beacon_threshold_ > second_clock::local_time() - when_last_beacon_seen_) {
        wait_time = no_beacon_threshold_ - (second_clock::local_time() - when_last_beacon_seen_);
    }

    return second_clock::local_time() + wait_time;
}

void BeaconParser::location_context_lock_name(const std::string& location_context_lock_name) {
    location_context_lock_name_ = location_context_lock_name;
}

void BeaconParser::no_beacon_threshold(const boost::posix_time::time_duration& no_beacon_threshold) {
    no_beacon_threshold_ = no_beacon_threshold;
}

void BeaconParser::parse_next_sample() {
    static FILE* stats_file = NULL;
    ptime lock_wait_deadline = calc_lock_wait_deadline();
    bool was_lock_acquired;

    if (stats_file == NULL) {
        stats_file = fopen("stats.txt", "w");
    }

    // Check that the BeaconParser was initialized before starting sample collection
    if (!sample0_lock_) {
        throw std::runtime_error("You must call init() before calling collect_next_sample()");
    }

    // Wait for the next sample to become available
    if (is_sample0_next_) {
        was_lock_acquired = sample0_lock_->timed_lock(lock_wait_deadline);
    } else {
        was_lock_acquired = sample1_lock_->timed_lock(lock_wait_deadline);
    }

    // If we were able to acquire the sample lock
    if (was_lock_acquired) {
        bool is_in_location;

        // Parse the beacon to see if we are in-location
        if (is_sample0_next_) {
            Beacon beacon = Beacon::create(sample0_file_path_);
            is_in_location = beacon.is_in_location();
            printf("%d %0.5f %0.5f %s\n", beacon.is_in_location(), beacon.hamming_distance(), beacon.hamming_distance_key(), beacon.key().c_str());
            fflush(stdout);
        } else {
            Beacon beacon = Beacon::create(sample1_file_path_);
            is_in_location = beacon.is_in_location();
            printf("%d %0.5f %0.5f %s\n", beacon.is_in_location(), beacon.hamming_distance(), beacon.hamming_distance_key(), beacon.key().c_str());
            fflush(stdout);
        }

        // If the beacon was successfully parsed
        if (is_in_location) {
            // Mark as the system as in-location
            set_is_in_location_context(is_in_location);

            // Update the last beacon seen time
            when_last_beacon_seen_ = second_clock::local_time();
        }

        // Toggle is sample next
        is_sample0_next_ = !is_sample0_next_;
    }

    // If the time elapsed since our last beacon has passed
    if (second_clock::local_time() - when_last_beacon_seen_ > no_beacon_threshold_) {
        // Mark the system as out of location context
        set_is_in_location_context(false);
    }
}

void BeaconParser::run() {
    BeaconParser parser;

    // Initialize the parser
    parser.init();

    // Keep parsing samples until the end of time
    while (true) {
        parser.parse_next_sample();
    }
}

/**
 * TODO(mmccann): This is a temporary hack. Boost::ipc doesn't expose the get_value function on its semaphores
 * because Windows doens't provide the equivalent. For the demo, I'm throwing in the non-portable code to get it
 * solved, but this will surely need to be fixed later.
 */
void BeaconParser::set_is_in_location_context(const bool is_location_context) {
    int current_value;
    sem_t* in_location_sem;
    const int not_in_location_context = 0;

    // Fetch the named semaphore
    in_location_sem = sem_open(
        location_context_lock_name_.c_str(), O_CREAT | O_RDWR, S_IRUSR | S_IWUSR, not_in_location_context);

    // Fetch the current value of the semaphore
    sem_getvalue(in_location_sem, &current_value);

    // If the state of the semaphore does not equal the desired value
    if (is_location_context != bool(current_value)) {
        // If the semaphore needs to be marked as in location
        if (is_location_context) {
            // Lock the semaphore to indicate in-location
            sem_post(in_location_sem);
        } else {
            // Unlock the semaphore to indicate out-of-location
            sem_wait(in_location_sem);
        }
    }
}

const ptime& BeaconParser::when_last_beacon_seen() const {
    return when_last_beacon_seen_;
}

void BeaconParser::when_last_beacon_seen(const ptime& when_last_beacon_seen) {
    when_last_beacon_seen_ = when_last_beacon_seen;
}
