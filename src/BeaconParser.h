/**
 * @blame R. Matt McCann <mccann.matt@gmail.com>
 * @brief This class continually parses audio samples as they are collected by the BeaconListener.
 * @copyright &copy; 2017 Evil Corp.
 */

#ifndef BEACON_PARSER_H_
#define BEACON_PARSER_H_

#include <boost/date_time/posix_time/posix_time.hpp>

#include "BeaconConsumer.h"

namespace evil_corp {

/**
 * This class continually parses audio samples collected by the BeaconParser.
 *
 * The parsers switches between two audio
 * recording files, waiting on a guard mutex to be unlocked by the BeaconParser, indicating their availability.
 *
 * The parser toggles the in-location semaphore to indicate presence within location if it is able to successfully
 * parse the audio samples.
 *
 * TODO: Break off the set_is_in_location_context out into a class representing the location-context lock.
 */
class BeaconParser : public BeaconConsumer {
public:
    /** Default time without successfully parsing a Beacon before marking the system as out of location (in ms) */
    static const size_t DEFAULT_NO_BEACON_THRESHOLD = 10000;

    BeaconParser();

    /** Parses the next audio sample and updates the location semaphore as appropriate. */
    void parse_next_sample();

    /** Creates a BeaconParser and then continually runs it. */
    static void run();

    /**
     * Marks the semaphore indicating that the system's in-location-context status.
     *
     * @param in_location_context If true, indicates the system is in-location
     */
    void set_is_in_location_context(const bool is_location_context);

    /*******************************************************************************************************************
    * Property Accessors
    *******************************************************************************************************************/
    void location_context_lock_name(const std::string& location_context_lock_name);

    void no_beacon_threshold(const boost::posix_time::time_duration& no_beacon_threshold);

    const boost::posix_time::ptime& when_last_beacon_seen() const;

    void when_last_beacon_seen(const boost::posix_time::ptime& when_last_beacon_seen);

private:
    /**
     * Calculates how long the BeaconParser should wait for an audio sample to become available. This value is variable
     * because we want to make sure the BeaconParser wakes up in time to enforse the no beacon threshold.
     *
     * @returns The time when the lock wait should give up
     */
    boost::posix_time::ptime calc_lock_wait_deadline();

    /** Name of the semaphore used to indicate location context proximity. */
    std::string location_context_lock_name_;

    /**
     * The amount of time that can elapse without successfully parsing a Beacon before the system is marked as
     * out of location.
     */
    boost::posix_time::time_duration no_beacon_threshold_;

    /** The time when the last beacon was seen. Used to determine if it's been too long since the last beacon. */
    boost::posix_time::ptime when_last_beacon_seen_;

}; // class BeaconParser

}; // namespace evil_corp

#endif // #ifndef BEACON_PARSER_H_