/***********************************************************************************************************************
 * @blame R. Matt McCann <mccann.matt@gmail.com>
 * @brief Implementation of wav_file.h
 * @copyright &copy; 2017 Evil Corp.
 **********************************************************************************************************************/

#include "WavFile.h"

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <samplerate.h>
#include <sndfile.hh>
#include <stdexcept>
#include <sstream>
#include <unistd.h>

using namespace evil_corp;
using namespace std;

#define	BUFFER_LEN		4096	/*-(1<<16)-*/

static sf_count_t sample_rate_convert (SNDFILE *infile, SNDFILE *outfile, int converter, double src_ratio, int channels, double * gain, int normalize) ;
static double apply_gain (float * data, long frames, int channels, double max, double gain) ;

WavFile WavFile::create(const std::string& file_path) {
    WavFile wav;
    SndfileHandle wav_file(file_path.c_str());

    // Check that the provided file is actually a WAV file
    if (!(wav_file.format() & SF_FORMAT_WAV)) {
        stringstream error_message;

        error_message << "The provided file (" << file_path << ") is not a WAV file";
        throw std::runtime_error(error_message.str());
    }

    // Read in the sampling frequency
    wav.sampling_frequency_ = wav_file.samplerate();

    // Resize the samples vector to be large enough to contain all of the sample data
    wav.samples_.resize(wav_file.frames());

    // Read the samples into the samples vector
    wav_file.read(&wav.samples_[0], wav_file.frames());

    // Calculate the duration of the WAV
    wav.duration_ = double(wav.samples_.size()) / wav.sampling_frequency_;

    return wav;
}

#ifdef ENABLE_PY_BINDINGS
#include <boost/python.hpp>

void WavFile::export_to_python() {
    boost::python::class_<WavFile>(
            "WavFile", boost::python::init<>()
        )
        .def("create", &WavFile::create)
        .def("resample", &WavFile::resample)
        .def("sampling_frequency", &WavFile::sampling_frequency)
        .staticmethod("create")
        .staticmethod("resample")
    ;
}
#endif

void WavFile::resample(const string& file_in_path, const string& file_out_path, const uint32_t sample_rate) {
    SNDFILE	*infile, *outfile = NULL ;
	SF_INFO sfinfo ;

	int normalize = 1 ;
	sf_count_t	count ;
	double		src_ratio = -1.0, gain = 1.0 ;
	int			new_sample_rate = sample_rate, k, converter, max_speed = SF_FALSE ;

	/* Set default converter. */
	converter = SRC_SINC_MEDIUM_QUALITY ;

	if ((infile = sf_open (file_in_path.c_str(), SFM_READ, &sfinfo)) == NULL)
	{
        printf ("Error : Not able to open input file '%s'\n", file_in_path.c_str()) ;
		exit (1) ;
	};

	printf ("Input File    : %s\n", file_in_path.c_str()) ;
	printf ("Sample Rate   : %d\n", sfinfo.samplerate) ;
	printf ("Input Frames  : %ld\n\n", (long) sfinfo.frames) ;

	if (new_sample_rate > 0)
	{
        src_ratio = (1.0 * new_sample_rate) / sfinfo.samplerate ;
		sfinfo.samplerate = new_sample_rate ;
	}
	else if (src_is_valid_ratio (src_ratio))
		sfinfo.samplerate = (int) floor (sfinfo.samplerate * src_ratio) ;
	else
	{
        printf ("Not able to determine new sample rate. Exiting.\n") ;
		sf_close (infile) ;
		return;
	} ;

	if (fabs (src_ratio - 1.0) < 1e-20)
	{
        printf ("Target samplerate and input samplerate are the same. Exiting.\n") ;
		sf_close (infile) ;
		return;
	} ;

	printf ("SRC Ratio     : %f\n", src_ratio) ;
	printf ("Converter     : %s\n\n", src_get_name (converter)) ;

	if (src_is_valid_ratio (src_ratio) == 0)
	{
        printf ("Error : Sample rate change out of valid range.\n") ;
		sf_close (infile) ;
		return;
	} ;

	printf ("Output file   : %s\n", file_out_path.c_str()) ;
	printf ("Sample Rate   : %d\n", sfinfo.samplerate) ;

	do
	{
        sf_close (outfile) ;

		if ((outfile = sf_open (file_out_path.c_str(), SFM_WRITE, &sfinfo)) == NULL)
		{
            printf ("Error : Not able to open output file '%s'\n", file_out_path.c_str()) ;
			sf_close (infile) ;
			return;
		} ;

		if (max_speed)
		{
            /* This is mainly for the comparison program tests/src-evaluate.c */
			sf_command (outfile, SFC_SET_ADD_PEAK_CHUNK, NULL, SF_FALSE) ;
		}
		else
		{
            /* Update the file header after every write. */
			sf_command (outfile, SFC_SET_UPDATE_HEADER_AUTO, NULL, SF_TRUE) ;
		} ;

		sf_command (outfile, SFC_SET_CLIPPING, NULL, SF_TRUE) ;

		count = sample_rate_convert (infile, outfile, converter, src_ratio, sfinfo.channels, &gain, normalize) ;
	}
	while (count < 0) ;

	printf ("Output Frames : %ld\n\n", (long) count) ;

	sf_close (infile) ;
	sf_close (outfile) ;

	return;
}

static sf_count_t
sample_rate_convert (SNDFILE *infile, SNDFILE *outfile, int converter, double src_ratio, int channels, double * gain, int normalize)
{	static float input [BUFFER_LEN] ;
	static float output [BUFFER_LEN] ;

	SRC_STATE	*src_state ;
	SRC_DATA	src_data ;
	int			error ;
	double		max = 0.0 ;
	sf_count_t	output_count = 0 ;

	sf_seek (infile, 0, SEEK_SET) ;
	sf_seek (outfile, 0, SEEK_SET) ;

	/* Initialize the sample rate converter. */
	if ((src_state = src_new (converter, channels, &error)) == NULL)
	{	printf ("\n\nError : src_new() failed : %s.\n\n", src_strerror (error)) ;
		exit (1) ;
		} ;

	src_data.end_of_input = 0 ; /* Set this later. */

	/* Start with zero to force load in while loop. */
	src_data.input_frames = 0 ;
	src_data.data_in = input ;

	src_data.src_ratio = src_ratio ;

	src_data.data_out = output ;
	src_data.output_frames = BUFFER_LEN /channels ;

	while (1)
	{
		/* If the input buffer is empty, refill it. */
		if (src_data.input_frames == 0)
		{	src_data.input_frames = sf_readf_float (infile, input, BUFFER_LEN / channels) ;
			src_data.data_in = input ;

			/* The last read will not be a full buffer, so snd_of_input. */
			if (src_data.input_frames < BUFFER_LEN / channels)
				src_data.end_of_input = SF_TRUE ;
			} ;

		if ((error = src_process (src_state, &src_data)))
		{	printf ("\nError : %s\n", src_strerror (error)) ;
			exit (1) ;
			} ;

		/* Terminate if done. */
		if (src_data.end_of_input && src_data.output_frames_gen == 0)
			break ;

		max = apply_gain (src_data.data_out, src_data.output_frames_gen, channels, max, *gain) ;

		/* Write output. */
		sf_writef_float (outfile, output, src_data.output_frames_gen) ;
		output_count += src_data.output_frames_gen ;

		src_data.data_in += src_data.input_frames_used * channels ;
		src_data.input_frames -= src_data.input_frames_used ;
		} ;

	src_delete (src_state) ;

	if (normalize && max > 1.0)
	{	*gain = 1.0 / max ;
		printf ("\nOutput has clipped. Restarting conversion to prevent clipping.\n\n") ;
		return -1 ;
		} ;

	return output_count ;
} /* sample_rate_convert */

static double
apply_gain (float * data, long frames, int channels, double max, double gain)
{
	long k ;

	for (k = 0 ; k < frames * channels ; k++)
	{	data [k] *= gain ;

		if (fabs (data [k]) > max)
			max = fabs (data [k]) ;
		} ;

	return max ;
} /* apply_gain */

itpp::vec WavFile::samples_as_itpp_vec() const {
    itpp::vec samples(samples_.size());

    for (size_t iter = 0; iter < samples_.size(); iter++) {
        samples[iter] = samples_[iter];
    }

    return samples;
}

itpp::cvec WavFile::samples_as_itpp_cvec() const {
    return itpp::to_cvec(samples_as_itpp_vec());
}
