/***********************************************************************************************************************
 * @blame R. Matt McCann <mccann.matt@gmail.com>
 * @brief Provides WAV file parsing routines
 * @copyright &copy; 2017 Evil Corp.
 **********************************************************************************************************************/

#ifndef AUDIO_LIBRARY__WAV_FILE_H_
#define AUDIO_LIBRARY__WAV_FILE_H_

#include <itpp/itbase.h>
#include <string>
#include <vector>


namespace evil_corp {

class WavFile {
public:
    /** Must use factory method to create WAV instances. */
    WavFile() = default;

    /*******************************************************************************************************************
     * Factory method; Creates a WAV file object from the specified audio sample.
     *
     * @param file_path Path of the file containing the WAV data
     * @returns The WAV file object representing the WAV file
     ******************************************************************************************************************/
     static WavFile create(const std::string& file_path);

     /** Exposes this class to Pythonland. */
     static void export_to_python();

     /**
      * Resamples the provided WAV file to produce the desired sample rate in the output file.
      *
      * @param file_in_path Path to the WAV file to be resampled
      * @param file_out_path Path to write out the resampled WAV file
      * @param sample_rate The desired sample rate in Hz
      */
     static void resample(const std::string& file_in_path, const std::string& file_out_path, uint32_t sample_rate);

     /******************************************************************************************************************
      * Property accessors.
      *****************************************************************************************************************/
     double duration() const { return duration_; }

     const std::vector<double>& samples() const { return samples_; }

     /** @returns Audio samples in itpp containers. */
     itpp::vec samples_as_itpp_vec() const;
     itpp::cvec samples_as_itpp_cvec() const;

     uint32_t sampling_frequency() const { return sampling_frequency_; }

private:

    /** Length of the WAV file in seconds. */
    double duration_;

    /** Audio samples comprising the WAV. */
    std::vector<double> samples_;

    /** Sampling frequency of the WAV. */
    uint32_t sampling_frequency_;

}; // class WavFile

}; // namespace evil_corp

#endif // #ifndef AUDIO_LIBRARY__WAV_FILE_H_