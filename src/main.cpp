/**
 * @blame R. Matt McCann <mccann.matt@gmail.com>
 * @brief Runs two threads, a beacon listener and a beacon parser, that work in tandem to maintain the Location
 *     context semaphore
 * @copyright &copy; 2017 Evil Corp.
 */

#include <boost/thread.hpp>
#include <iostream>

#include "BeaconListener.h"
#include "BeaconParser.h"

using namespace evil_corp;

int main(int argc, char** argv) {
    // Launch the dynamic duo
    boost::thread listener_thread(&BeaconListener::run);
    boost::thread parser_thread(&BeaconParser::run);

    // Wait forever!
    listener_thread.join();
    parser_thread.join();
}