/**
 * @blame R. Matt McCann
 * @brief Packages up the Python exposed classes
 * @copyright &copy; 2017 Evil Corp.
 */

#include <boost/python.hpp>

#include "Beacon.h"
#include "WavFile.h"

using namespace boost::python;
using namespace evil_corp;

BOOST_PYTHON_MODULE(libreceiver) {
    Beacon::export_to_python();
    WavFile::export_to_python();
}