/**
 * @blame R. Matt McCann <mccann.matt@gmail.com>
 * @brief Test runner for the C++ receiver code
 * @copyright &copy; 2017 Evil Corp.
 */
#include <gtest/gtest.h>

#include "test_BeaconConsumer.h"
#include "test_BeaconListener.h"
#include "test_BeaconParser.h"
#include "test_Beacon.h"
#include "test_WavFile.h"


int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}