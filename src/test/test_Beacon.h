#ifndef TEST__TEST_BEACON_H_
#define TEST__TEST_BEACON_H_

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <vector>

#include "Beacon.h"
#include "WavFile.h"
#include "test_helper.h"

using namespace evil_corp;
using namespace itpp;
using namespace std;


TEST(Beacon, test_calc_hamming_distance__equal) {
    Beacon beacon = Beacon::create("src/test/data/key.wav");

    // Copy an exact copy of the expected into the data packet vector
    vector<uint8_t> data_packet(sizeof(Beacon::EXPECTED_DATA_PACKET));
    memcpy(&data_packet[0], Beacon::EXPECTED_DATA_PACKET, data_packet.size());
    beacon.data_packet(data_packet);

    // Calculate the hamming distance
    beacon.calc_hamming_distance();

    // Verify the hamming distance is 0
    EXPECT_EQ(beacon.hamming_distance(), 0);
}

TEST(Beacon, test_calc_hamming_distance__not_equal__same_length) {
    Beacon beacon = Beacon::create("src/test/data/key.wav");

    // Copy an exact copy of the expected into the data packet vector
    vector<uint8_t> data_packet(sizeof(Beacon::EXPECTED_DATA_PACKET));
    memcpy(&data_packet[0], Beacon::EXPECTED_DATA_PACKET, data_packet.size());
    for (size_t iter = 0; iter < data_packet.size(); iter += 5) {
        if (data_packet[iter]) {
            data_packet[iter] = 0;
        } else {
            data_packet[iter] = 1;
        }
    }
    beacon.data_packet(data_packet);

    // Calculate the hamming distance
    beacon.calc_hamming_distance();

    // Verify the hamming distance is 0
    EXPECT_NEAR(beacon.hamming_distance(), 0.200658, 1e-6);
}

TEST(Beacon, test_no_noise_key_file) {
    Beacon beacon = Beacon::create("src/test/data/key.wav");
    vector<uint8_t> expected_data_packet;
    double expected_correction_factor = 1.0;
    double expected_frequency_shift = -3.086509427904588e-07;
    int expected_pilot_start_sample = 7154;
    int expected_signal_start_sample = 13012;
    vec expected_time_signal;

    // Load expected time signal
    load_test_data_from_file("src/test/data/expected_time.txt", expected_time_signal);

    // Check that the correction value is correct
    EXPECT_NEAR(beacon.correction, expected_correction_factor, 1e-5);

    // Check that the synchronization derived values are correct
    EXPECT_EQ(beacon.pilotStartSample, expected_pilot_start_sample);
    EXPECT_EQ(beacon.signalStartSample, expected_signal_start_sample);

    // Check that data packet matches the expected values
    for (size_t iter = 0; iter < beacon.data_packet().size(); iter ++ ) {
        EXPECT_EQ(beacon.data_packet()[iter], Beacon::EXPECTED_DATA_PACKET[iter]);
    }

    // Check that the hamming distance is 0
    EXPECT_EQ(beacon.hamming_distance(), 0);

    // TODO(mmccann) - Reenable this to test the decode_data_packet function
    // Check that the decoded key is hello
    //EXPECT_EQ(beacon.key(), "hello");

    // Check that the beacon is marked as in-location
    EXPECT_TRUE(beacon.is_in_location());
}

TEST(Beacon, test_noisy_key_file) {
    Beacon beacon = Beacon::create("src/test/data/noisy_key.wav");

    // Check that the hamming distance is 0
    EXPECT_NEAR(beacon.hamming_distance(), 0.029605, 0.0001);

    // TODO(mmccann) - Reenable this to test the decode_data_packet function
    // Check that the decoded key is hello
    //EXPECT_EQ(beacon.key(), "hello");

    // Check that the beacon is marked as in-location
    EXPECT_TRUE(beacon.is_in_location());
}

TEST(Beacon, test_multiple_keys_file) {
    Beacon beacon = Beacon::create("src/test/data/multiple_keys.wav");

    // Check that the hamming distance is 0
    EXPECT_EQ(beacon.hamming_distance(), 0);

    // TODO(mmccann) - Reenable this to test the decode_data_packet function
    // Check that the decoded key is hello
    //EXPECT_EQ(beacon.key(), "hello");

    // Check that the beacon is marked as in-location
    EXPECT_TRUE(beacon.is_in_location());
}

TEST(Beacon, test_multiple_noisy_keys_file) {
    Beacon beacon = Beacon::create("src/test/data/multiple_noisy_keys.wav");

    // Check that the hamming distance is 0
    EXPECT_NEAR(beacon.hamming_distance(), 0.00328947, 0.000001);

    // TODO(mmccann) - Reenable this to test the decode_data_packet function
    // Check that the decoded key is hello
    //EXPECT_EQ(beacon.key(), "hello");

    // Check that the beacon is marked as in-location
    EXPECT_TRUE(beacon.is_in_location());
}

#endif
