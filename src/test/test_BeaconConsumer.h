/**
 * @blame R. Matt McCann <mccann.matt@gmail.com>
 * @brief Unit tests for the BeaconConsumer class.
 * @copyright &copy;
 */

#ifndef TEST__BEACON_CONSUMER_H
#define TEST__BEACON_CONSUMER_H

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "BeaconConsumer.h"


using namespace boost::interprocess;
using namespace evil_corp;


/** This test confirms the mutexes are initialized and properly locked. */
TEST(BeaconConsumer, test_initialize) {
    BeaconConsumer consumer;
    const char* sample0_lock_name = "beacon_listener_test_initialize0";
    const char* sample1_lock_name = "beacon_listener_test_initialize1";

    // Initialize the consumer
    consumer.sample0_lock_name(sample0_lock_name);
    consumer.sample1_lock_name(sample1_lock_name);
    consumer.init();

    // Let's fetch our own copy of the mutexes
    named_mutex sample0_lock(open_only_t(), sample0_lock_name);
    named_mutex sample1_lock(open_only_t(), sample1_lock_name);

    // Confirm that the sample locks are locked
    EXPECT_FALSE(sample0_lock.try_lock());
    EXPECT_FALSE(sample0_lock.try_lock());

    // Let's clean up the allocated mutexes now that we are done testing
    named_mutex::remove(sample0_lock_name);
    named_mutex::remove(sample1_lock_name);
}

#endif // #ifndef TEST__BEACON_CONSUMER_H