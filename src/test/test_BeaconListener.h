/**
 * @blame R. Matt McCann <mccann.matt@gmail.com>
 * @brief Unit tests for the BeaconListener class
 * @copyright &copy; 2017 Evil Corp.
 */

#ifndef TEST__TEST_BEACON_LISTENER_H
#define TEST__TEST_BEACON_LISTENER_H

#include <boost/filesystem.hpp>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "BeaconListener.h"
#include "WavFile.h"


using namespace boost::filesystem;
using namespace boost::interprocess;
using namespace evil_corp;
using namespace std;


/** Tests trying to collect the next sample before the listener has been initialized. */
TEST(BeaconListener, test_collect_next_sample__not_initialized) {
    BeaconListener listener;

    try {
        listener.collect_next_sample();
        FAIL();
    } catch (runtime_error& ex) {
        SUCCEED();
    }
}

/** Tests handling of a failed recording when collecting the next audo sample. */
TEST(BeaconListener, test_collect_next_sample__audio_record_failure) {
    cerr << "Implement me!" << endl;
}

/** Tests collecting the next sample. */
/*
TODO(mccann) - The way this test is written, it's trying to access the sound card hardware, which is not available
on gitlab test runners
TEST(BeaconListener, test_collect_next_sample) {
    BeaconListener listener;
    string sample0_file_path = "/tmp/BeaconListener.test_collect_next_sample.0.wav";
    const char* sample0_lock_name = "beacon_listener_sample0_is_next";
    string sample1_file_path = "/tmp/BeaconListener.test_collect_next_sample.1.wav";
    const char* sample1_lock_name = "beacon_listener_sample1_is_next";

    // Initialize the listener
    listener.sampling_duration(1);
    listener.sample0_file_path(sample0_file_path);
    listener.sample0_lock_name(sample0_lock_name);
    listener.sample1_file_path(sample1_file_path);
    listener.sample1_lock_name(sample1_lock_name);
    listener.init();

    // Let's fetch our own copy of the sample1 mutex
    named_mutex sample0_lock(open_only_t(), sample0_lock_name);
    named_mutex sample1_lock(open_only_t(), sample1_lock_name);

    // Collect sample 0
    listener.collect_next_sample();

    // Verify that the sample lock was unlocked
    EXPECT_TRUE(sample0_lock.try_lock());

    // Verify that the sample file was placed as expected
    EXPECT_TRUE(is_regular_file(sample0_file_path));

    // Verify that the sample file is valid WAV
    WavFile wav = WavFile::create(sample0_file_path);
    EXPECT_EQ(wav.duration(), 1);
    EXPECT_EQ(wav.samples().size(), listener.sampling_rate());
    EXPECT_EQ(wav.sampling_frequency(), listener.sampling_rate());

    // Collect sample 1
    listener.collect_next_sample();

    // Verify that the sample lock was unlocked
    EXPECT_TRUE(sample1_lock.try_lock());

    // Verify that sample file was placed as expected
    EXPECT_TRUE(is_regular_file(sample1_file_path));

    // Verify that the sample file was placed as expected
    WavFile wav2 = WavFile::create(sample1_file_path);
    EXPECT_EQ(wav2.duration(), 1);
    EXPECT_EQ(wav2.samples().size(), listener.sampling_rate());
    EXPECT_EQ(wav2.sampling_frequency(), listener.sampling_rate());
}*/

#endif // #ifndef TEST__TEST_BEACON_LISTENER_H