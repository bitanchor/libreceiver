/**
 * @blame R. Matt McCann <mccann.matt@gmail.com>
 * @brief Unit tests for the BeaconParser class.
 * @copyright &copy; 2017 Evil Corp.
 */

#ifndef TEST__TEST_BEACON_PARSER_H
#define TEST__TEST_BEACON_PARSER_H

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "BeaconParser.h"


using namespace boost::posix_time;
using namespace evil_corp;
using namespace std;


/** Tests trying to parse the next sample before the listener has been initialized. */
TEST(BeaconParser, parse_next_sample__not_initialized) {
    BeaconParser parser;

    try {
        parser.parse_next_sample();
        FAIL();
    } catch(runtime_error& ex) {
        SUCCEED();
    }
}

/** Tests trying to parse the next sample, but no sample lock was acquired in the permitted time. */
TEST(BeaconParser, parse_next_sample__no_lock_acquired) {
    BeaconParser parser;
    const char* sample0_lock_name = "test_beacon_parser__no_lock_acquired0";
    const char* sample1_lock_name = "test_beacon_parser__no_lock_acquired1";

    // Clean up any dangling mutex locks
    named_mutex::remove(sample0_lock_name);
    named_mutex::remove(sample1_lock_name);

    // Initialize the parser
    parser.sample0_lock_name(sample0_lock_name);
    parser.sample1_lock_name(sample1_lock_name);
    parser.no_beacon_threshold(seconds(0));
    parser.init();

    // If the lock acquisition failure is ignored, the parser will try to open a sample file that does not exist,
    // thus blowing up. As long as this doens't happen, the test has passed
    parser.parse_next_sample();
    parser.parse_next_sample();
}

/**
 * Tests parsing the sample, but the sample is not successfully parsed.
 * TODO(mmccann) - These tests really aught to use mocks, but I haven't the inclination at the moment to look gmocks
 */
TEST(BeaconParser, parse_next_sample__beacon_parse_fails) {
    int current_value;
    sem_t* location_sem;
    const char* lock_name = "/beacon_parser__parse_next_sample__beacon_parse_fails";
    const int in_location_context = 1;
    BeaconParser parser;
    const char* sample0_file_path = "./src/test/data/no_key.wav";
    const char* sample1_file_path = "./src/test/data/no_key.wav";
    const char* sample0_lock_name = "parse_next_sample__beacon_parse_fails0";
    const char* sample1_lock_name = "parse_next_sample__beacon_parse_fails1";
    ptime when_last_beacon_seen = second_clock::local_time() - microseconds(1);

    // Clean up any dangling sample locks from previous runs of the test
    named_mutex::remove(sample0_lock_name);
    named_mutex::remove(sample1_lock_name);

    // Initialize the parser
    parser.sample0_file_path(sample0_file_path);
    parser.sample0_lock_name(sample0_lock_name);
    parser.sample1_file_path(sample1_file_path);
    parser.sample1_lock_name(sample1_lock_name);
    parser.location_context_lock_name(lock_name);
    parser.init();

    // Let's fetch our own copy of the sample mutexes
    named_mutex sample0_lock(open_only_t(), sample0_lock_name);
    named_mutex sample1_lock(open_only_t(), sample1_lock_name);

    // Mark both samples as available
    sample0_lock.unlock();
    sample1_lock.unlock();

    // Fetch and initialize the location semaphore
    sem_unlink(lock_name);
    location_sem = sem_open(lock_name, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR, in_location_context);

    // Parse the next beacon file
    parser.when_last_beacon_seen(when_last_beacon_seen);
    parser.parse_next_sample();

    // Test that the location context semaphore has not been disabled (not enough time elapsed)
    sem_getvalue(location_sem, &current_value);
    EXPECT_EQ(current_value, 1);

    // Test that the last beacon seen timestamp was not updated
    EXPECT_EQ(parser.when_last_beacon_seen(), when_last_beacon_seen);

    // Test that the sample lock is locked
    EXPECT_FALSE(sample0_lock.try_lock());

    // Test that the next sample is toggled
    EXPECT_FALSE(parser.is_sample0_next());

    // Parse the next sample
    when_last_beacon_seen = second_clock::local_time() - seconds(30);
    parser.when_last_beacon_seen(when_last_beacon_seen);
    parser.parse_next_sample();

    // Test that the location context semaphore has been disabled
    sem_getvalue(location_sem, &current_value);
    EXPECT_EQ(current_value, 0);

    // Test that the last beacon seen timestamp was not updated
    EXPECT_EQ(parser.when_last_beacon_seen(), when_last_beacon_seen);

    // Test that the sample lock is locked
    EXPECT_FALSE(sample1_lock.try_lock());

    // Test that the next sample is toggled
    EXPECT_TRUE(parser.is_sample0_next());
}

TEST(BeaconParser, parse_next_sample__beacon_parse_succeeds) {
    int current_value;
    sem_t* location_sem;
    const char* lock_name = "/beacon_parser__parse_next_sample__beacon_parse_succeeds";
    const int not_in_location_context = 0;
    BeaconParser parser;
    const char* sample0_file_path = "./src/test/data/key.wav";
    const char* sample1_file_path = "./src/test/data/key.wav";
    const char* sample0_lock_name = "parse_next_sample__beacon_parse_succeeds0";
    const char* sample1_lock_name = "parse_next_sample__beacon_parse_succeeds1";
    ptime when_last_beacon_seen = second_clock::local_time() - microseconds(1);

    // Initialize the parser
    parser.sample0_file_path(sample0_file_path);
    parser.sample0_lock_name(sample0_lock_name);
    parser.sample1_file_path(sample1_file_path);
    parser.sample1_lock_name(sample1_lock_name);
    parser.location_context_lock_name(lock_name);
    parser.init();

    // Let's fetch our own copy of the sample mutexes
    named_mutex sample0_lock(open_only_t(), sample0_lock_name);
    named_mutex sample1_lock(open_only_t(), sample1_lock_name);

    // Mark both samples as available
    sample0_lock.unlock();
    sample1_lock.unlock();

    // Fetch and initialize the location semaphore
    sem_unlink(lock_name);
    location_sem = sem_open(lock_name, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR, not_in_location_context);

    // Parse the next beacon file
    parser.parse_next_sample();

    // Test that the location context semaphore has been enabled
    sem_getvalue(location_sem, &current_value);
    EXPECT_EQ(current_value, 1);

    // Test that the last beacon seen timestamp was not updated
    EXPECT_NE(parser.when_last_beacon_seen(), when_last_beacon_seen);

    // Test that the sample lock is locked
    EXPECT_FALSE(sample0_lock.try_lock());

    // Test that the next sample is toggled
    EXPECT_FALSE(parser.is_sample0_next());

    // Mark the location context semaphore as out of context
    sem_wait(location_sem);

    // Parse the next sample
    parser.when_last_beacon_seen(when_last_beacon_seen);
    parser.parse_next_sample();

    // Test that the location context semaphore has been enabled
    sem_getvalue(location_sem, &current_value);
    EXPECT_EQ(current_value, 1);

    // Test that the last beacon seen timestamp was not updated
    EXPECT_NE(parser.when_last_beacon_seen(), when_last_beacon_seen);

    // Test that the sample lock is locked
    EXPECT_FALSE(sample1_lock.try_lock());

    // Test that the next sample is toggled
    EXPECT_TRUE(parser.is_sample0_next());
}

/** Tests setting the location-context lock */
TEST(BeaconParser, set_is_in_location_context) {
    int current_value;
    sem_t* location_sem;
    const char* lock_name = "/beacon_parser__set_is_in_location_context";
    const int not_in_location_context = 0;
    BeaconParser parser;

    // Initialize the parser
    parser.location_context_lock_name(lock_name);
    parser.init();

    // Fetch and initialize the location semaphore
    location_sem = sem_open(lock_name, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR, not_in_location_context);
    sem_getvalue(location_sem, &current_value);
    while (current_value) {
        sem_wait(location_sem);
        sem_getvalue(location_sem, &current_value);
    }

    // Try to mark the semaphore as in-location when it is already marked as in-location
    sem_post(location_sem);
    parser.set_is_in_location_context(true);
    sem_getvalue(location_sem, &current_value);
    ASSERT_EQ(current_value, 1);

    // Try to mark the semaphore as in-location when it is not already marked as in-location
    sem_wait(location_sem);
    parser.set_is_in_location_context(true);
    sem_getvalue(location_sem, &current_value);
    ASSERT_EQ(current_value, 1);

    // Try to mark the semaphore as out-location when it is marked as in-location
    parser.set_is_in_location_context(false);
    sem_getvalue(location_sem, &current_value);
    ASSERT_EQ(current_value, 0);

    // Try to mark the semaphore as out-location when it is already marked as out-location
    parser.set_is_in_location_context(false);
    sem_getvalue(location_sem, &current_value);
    ASSERT_EQ(current_value, 0);
}

#endif // #ifndef TEST__TEST_BEACON_PARSER_H