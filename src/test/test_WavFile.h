/***********************************************************************************************************************
 * @blame R. Matt McCann <mccann.matt@gmail.com>
 * @brief Unit tests for WavFile
 * @copyright &copy; 2017 Evil Corp.
 **********************************************************************************************************************/

#ifndef TEST__TEST_WAV_FILE_H_
#define TEST__TEST_WAV_FILE_H_

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "WavFile.h"


using namespace evil_corp;
using namespace std;

/** Simple test of the WavFile's file parsing behavior. */
TEST(WavFile, test_create) {
    WavFile wav_file = WavFile::create("src/test/data/key.wav");

    EXPECT_NEAR(wav_file.duration(), 0.72978186321204042, 1e-10);
    EXPECT_EQ(wav_file.sampling_frequency(), 41992);
    EXPECT_EQ(wav_file.samples().size(), 30645);
}

#endif // #ifndef TEST__TEST_WAV_FILE_H_