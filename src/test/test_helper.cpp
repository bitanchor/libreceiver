#include "test_helper.h"

namespace evil_corp {

void dump_vec(const itpp::vec& data, const std::string& file_name) {
    FILE* file = fopen(file_name.c_str(), "w");
    fprintf(file, "%d\n", data.length());
    for (size_t iter = 0; iter < data.length(); iter++) {
        fprintf(file, "%5.15lf\n", data[iter]);
    }
    fclose(file);
}

void dump_cvec(const itpp::cvec& data, const std::string& file_name) {
    FILE* file = fopen(file_name.c_str(), "w");
    fprintf(file, "%d\n", data.length());
    for (size_t iter = 0; iter < data.length(); iter++) {
        fprintf(file, "%5.15lf %5.15lf\n", data[iter].real(), data[iter].imag());
    }
    fclose(file);
}

/***********************************************************************************************************************
 * Loads a vector of test data from the specified file. Expects test file to contain:
 *
 * [length of vector] [elem1] [elem2] ... [elem2]
 *
 * @param file_path (in) Path of the file containing the test data
 * @param test_data (out) Test data loaded from the file
 **********************************************************************************************************************/
void load_test_data_from_file(const std::string& file_path, std::vector<std::complex<double> >& test_data) {
    std::ifstream file(file_path);
    std::string line;
    size_t test_data_length;

    // Read in the test data Length
    getline(file, line);
    sscanf(line.c_str(), "%lu", &test_data_length);

    // Read in the test data
    while(test_data_length-- > 0) {
        double imag;
        double real;

        // Read in the next element
        getline(file, line);

        // Parse the next element
        sscanf(line.c_str(), "%lf %lf", &real, &imag);

        // Add the test data element to the test data collection
        test_data.push_back(std::complex<double>(real,imag));
    }
} // load_test_data_from_file

void load_test_data_from_file(const std::string& file_path, std::vector<double>& test_data) {
    std::ifstream file(file_path);
    std::string line;
    size_t test_data_length;

    // Read in the test data Length
    getline(file, line);
    sscanf(line.c_str(), "%lu", &test_data_length);

    // Read in the test data
    while(test_data_length-- > 0) {
        double value;

        // Read in the next element
        getline(file, line);

        // Parse the next element
        sscanf(line.c_str(), "%lf", &value);

        // Add the test data element to the test data collection
        test_data.push_back(value);
    }
} // load_test_data_from_file

void load_test_data_from_file(const std::string& file_path, std::vector<uint8_t>& test_data) {
    std::ifstream file(file_path);
    std::string line;
    size_t test_data_length;

    // Read in the test data Length
    getline(file, line);
    sscanf(line.c_str(), "%lu", &test_data_length);

    // Read in the test data
    while(test_data_length-- > 0) {
        int value;

        // Read in the next element
        getline(file, line);

        // Parse the next element
        sscanf(line.c_str(), "%d", &value);

        // Add the test data element to the test data collection
        test_data.push_back(uint8_t(value));
    }
} // load_test_data_from_file

void load_test_data_from_file(const std::string& file_path, itpp::vec& test_data) {
    std::vector<double> test_data__std;

    // Load the test data into an std vector
    load_test_data_from_file(file_path, test_data__std);

    // Resize the itpp vector to be large enough for the test data
    test_data.set_size(test_data__std.size());

    // Copy the std vector elements into the itpp vector
    for (size_t iter = 0; iter < test_data__std.size(); iter++) {
        test_data[iter] = test_data__std[iter];
    }
}

void load_test_data_from_file(const std::string& file_path, itpp::cvec& test_data) {
    std::vector<std::complex<double> > test_data__std;

    // Load the test data into an std vector
    load_test_data_from_file(file_path, test_data__std);

    // Resize the itpp vector to be large enough for the test data
    test_data.set_size(test_data__std.size());

    // Copy the std vector elements into the itpp vector
    for (size_t iter = 0; iter < test_data__std.size(); iter++) {
        test_data[iter] = test_data__std[iter];
    }
}

}; // namespace evil_corp;