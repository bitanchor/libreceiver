#ifndef TEST__TEST_HELPER_H_
#define TEST__TEST_HELPER_H_

#include <cstdio>
#include <fstream>
#include <itpp/itbase.h>
#include <sndfile.hh>
#include <vector>


namespace evil_corp {

void dump_vec(const itpp::vec& data, const std::string& file_name);
void dump_cvec(const itpp::cvec& data, const std::string& file_name);

/***********************************************************************************************************************
 * Loads a vector of test data from the specified file. Expects test file to contain:
 *
 * [length of vector] [elem1] [elem2] ... [elem2]
 *
 * @param file_path (in) Path of the file containing the test data
 * @param test_data (out) Test data loaded from the file
 **********************************************************************************************************************/
void load_test_data_from_file(const std::string& file_path, std::vector<std::complex<double> >& test_data);
void load_test_data_from_file(const std::string& file_path, std::vector<double>& test_data);
void load_test_data_from_file(const std::string& file_path, std::vector<uint8_t>& test_data);
void load_test_data_from_file(const std::string& file_path, itpp::vec& test_data);
void load_test_data_from_file(const std::string& file_path, itpp::cvec& test_data);
}; // namespace evil_corp

#endif // #ifndef TEST__TEST_HELPER_H_