#!/usr/bin/env python3

# @blame R. Matt McCann
# @brief Helper script to facilitate using the gitlab-runner to test CI
# @copyright &copy; 2017 Human-Proof Corp.

import os
import sys

# Check the command-line usage
if len(sys.argv) != 2:
    print("Usage: %s (job_name)" % sys.argv[0])
    sys.exit(-1)
job = sys.argv[1]

# Grab the needed configuration settings from the user's environment
if 'HUMANPROOF_DOCKER_USER' not in os.environ:
    print("You must define the environment variable HUMANPROOF_DOCKER_USER.")
    print("  This variable should be set to your gitlab user account used to access the Human-Proof repos")
    print("  Consider adding this variable to ~/.bashrc - that's what the cool kids do!")
    sys.exit(-1)
docker_user = os.environ.get('HUMANPROOF_DOCKER_USER')

if 'HUMANPROOF_DOCKER_ACCESS_TOKEN' not in os.environ:
    print("You must define the environment variable HUMANPROOF_DOCKER_ACCESS_TOKEN")
    print("  This variable should be set to your gitlab user account access token")
    print("  Visit https://gitlab.com/profile/personal_access_tokens to generate the token")
    print("  Note - you must give this access token both api and read_registry scopes to run all stages")
    print("  Consider adding this variable to ~/.bashrc - that's what the cool kids do!")
    sys.exit(-1)
docker_access_token = os.environ.get('HUMANPROOF_DOCKER_ACCESS_TOKEN')

if 'HUMANPROOF_BUILD_REF_NAME' not in os.environ:
    print("You must define the environment variable HUMANPROOF_BUILD_REF_NAME")
    print("  This variable can be set to any value that is unique amongst the team e.g. matt-laptop")
    print("  This variable is used to tag your image builds when testing locally")
    print("  Consider adding this variable to ~/.bashrc - that's what the cool kids do!")
    sys.exit(-1)
build_ref_name = os.environ.get('HUMANPROOF_BUILD_REF_NAME')

if 'HUMANPROOF_AWS_ACCESS_KEY_ID' not in os.environ:
    print("You must define the environment variable HUMANPROOF_AWS_ACCESS_KEY_ID")
    print("  This variable should be set to the access key id used for accessing the Human-Proof AWS")
    print("  Contact Matt (mccann.matt@gmail.com) for this value")
    print("  Consider adding this variable to ~/.bashrc - that's what the cool kids do!")
    sys.exit(-1)
aws_access_key_id = os.environ.get('HUMANPROOF_AWS_ACCESS_KEY_ID')

if 'HUMANPROOF_AWS_ACCESS_SECRET_KEY' not in os.environ:
    print("You must define the environment variable HUMANPROOF_AWS_ACCESS_SECRET_KEY")
    print("  This variable should be set to the secret access key used for accessing the Human-Proof AWS")
    print("  Contact Matt (mccann.matt@gmail.com) for this value")
    print("  Consider adding this variable to ~/.bashrc - that's what the cool kids do!")
    sys.exit(-1)
aws_access_secret_key = os.environ.get('HUMANPROOF_AWS_ACCESS_SECRET_KEY')

# Build the gitlab-runner command
command = 'gitlab-runner exec docker '\
    '--env DOCKER_USER=%s '\
    '--env CI_BUILD_TOKEN=%s '\
    '--env CI_BUILD_REF_NAME=%s '\
    '--env AWS_ACCESS_KEY_ID=%s '\
    '--env AWS_SECRET_ACCESS_KEY=%s '\
    '--docker-privileged %s'
command_pub = command % (docker_user, "********", build_ref_name, aws_access_key_id, "********", job)
command = command % (docker_user, docker_access_token, build_ref_name, aws_access_key_id, aws_access_secret_key, job)


# Run the gitlab-runner command
print("Running", command_pub)
os.system(command)
